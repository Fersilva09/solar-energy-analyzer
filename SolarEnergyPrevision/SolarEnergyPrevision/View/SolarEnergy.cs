﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SolarEnergyPrevision
{


    public partial class SolarEnergy : Form
    {






        DataController dataController;

 
   



        public SolarEnergy()
        {
            InitializeComponent();
            tabControlSolarEnergy.Enabled = false;

            dataController = new DataController();
            dataController.StartCapture();
            dataController.GetPrevisionData();

            dataGridViewDataRead.Rows.Clear();
            dataGridViewDataRead.Refresh();
            dataGridViewDataRead.Visible = false;

          
        }


        #region FORMFUNCTIONS

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.dataController.StopCapture();
        }

        #endregion

        private void buttonSetData_Click(object sender, EventArgs e)
        {
            DateTime startDateTime = dateTimePickerStartDate.Value.Date + dateTimePickerStartTime.Value.TimeOfDay;
            DateTime endDateTime = dateTimePickerEndDate.Value.Date + dateTimePickerEndTime.Value.TimeOfDay;

            dataGridViewDataRead.Rows.Clear();
            dataGridViewDataRead.Refresh();
            dataGridViewDataRead.Visible = false;

            if(!dataController.SetDataReader(startDateTime, endDateTime))
            {
                MessageBox.Show("Data Invalida: não há dados disponíveis para essa data", "Error");
                tabControlSolarEnergy.Enabled = false;
            }
            else
            {
                tabControlSolarEnergy.Enabled = true;
            }

        }

        private void buttonGetData_Click(object sender, EventArgs e)
        {

            DateTime startDateTime = dateTimePickerStartDate.Value.Date + dateTimePickerStartTime.Value.TimeOfDay;
            DateTime endDateTime = dateTimePickerEndDate.Value.Date + dateTimePickerEndTime.Value.TimeOfDay;
            dataGridViewDataRead.Rows.Clear();
            dataGridViewDataRead.Refresh();
            dataGridViewDataRead.Visible = true;

            dataGridViewDataRead.ColumnCount = 14;

            dataGridViewDataRead.Columns[0].HeaderText = "Data e Horario";
            dataGridViewDataRead.Columns[1].HeaderText = "Radiacao";
            dataGridViewDataRead.Columns[2].HeaderText = "Potencia";
            dataGridViewDataRead.Columns[3].HeaderText = "Temperatura";
            dataGridViewDataRead.Columns[4].HeaderText = "Indice UV";
            dataGridViewDataRead.Columns[5].HeaderText = "Preciptacao";
            dataGridViewDataRead.Columns[6].HeaderText = "Nuvens";
            dataGridViewDataRead.Columns[7].HeaderText = "Velocidade do Vento";
            dataGridViewDataRead.Columns[8].HeaderText = "Sensacao Termica";
            dataGridViewDataRead.Columns[9].HeaderText = "Humidade do Ar";
            dataGridViewDataRead.Columns[10].HeaderText = "Pressao Reduzida";
            dataGridViewDataRead.Columns[11].HeaderText = "Rajada";
            dataGridViewDataRead.Columns[12].HeaderText = "Preciptacao Acumulada";
            dataGridViewDataRead.Columns[13].HeaderText = "Visibilidade";

            for(int counter = 0; counter < dataController.dataDateTime.Count; counter++)
            {
                dataGridViewDataRead.Rows.Add(
                    dataController.dataDateTime[counter],
                    dataController.radiation[counter],
                    dataController.potency[counter],
                    dataController.temperature[counter],
                    dataController.uvRadiation[counter],
                    dataController.rainChance[counter],
                    dataController.clouds[counter],
                    dataController.windSpeed[counter],
                    dataController.thermalSensation[counter],
                    dataController.airHumidity[counter],
                    dataController.reducPression[counter],
                    dataController.wind[counter],
                    dataController.accRain[counter],
                    dataController.visibility[counter]

                    );
            }

            
            dataGridViewDataRead.Columns[1].Visible = checkBoxDataRadiation.Checked;            
            dataGridViewDataRead.Columns[2].Visible = checkBoxDataPot.Checked;            
            dataGridViewDataRead.Columns[3].Visible = checkBoxPrevisionTemp.Checked;          
            dataGridViewDataRead.Columns[4].Visible = checkBoxPrevisionUVRadiation.Checked;           
            dataGridViewDataRead.Columns[5].Visible = checkBoxPrevisionRainChance.Checked;
            dataGridViewDataRead.Columns[6].Visible = checkBoxPrevisionClouds.Checked;            
            dataGridViewDataRead.Columns[7].Visible = checkBoxPrevisionWindSpeed.Checked;            
            dataGridViewDataRead.Columns[8].Visible = checkBoxPrevisionThermalSensation.Checked;          
            dataGridViewDataRead.Columns[9].Visible = checkBoxPrevisionAirHumidity.Checked;            
            dataGridViewDataRead.Columns[10].Visible = checkBoxPrevisionReducPression.Checked;           
            dataGridViewDataRead.Columns[11].Visible = checkBoxPrevisionWind.Checked;                      
            dataGridViewDataRead.Columns[12].Visible = checkBoxPrevisionAccRain.Checked;           
            dataGridViewDataRead.Columns[13].Visible = checkBoxPrevisionVisibility.Checked;
            
        }

    }
       
}
