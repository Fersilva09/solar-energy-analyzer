﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarEnergyPrevision
{
    public class PrevisionData
    {
        public String time;
        public String clouds;
        public String acPrec;
        public String wind;
        public String temp;
        public String termSensation;
        public String relativeHumidity;
        public String rainChance;
        public String windStrong;
        public String reducPression;
        public String radiation;
        public String visibility;
    }
}
