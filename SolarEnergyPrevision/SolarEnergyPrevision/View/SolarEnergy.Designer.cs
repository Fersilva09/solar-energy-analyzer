﻿namespace SolarEnergyPrevision
{
    partial class SolarEnergy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerEndTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.buttonSetData = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControlSolarEnergy = new System.Windows.Forms.TabControl();
            this.tabPageData = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxPrevisionRainChance = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionClouds = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionWindSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionUVRadiation = new System.Windows.Forms.CheckBox();
            this.checkBoxDataRadiation = new System.Windows.Forms.CheckBox();
            this.checkBoxDataPot = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionThermalSensation = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionTemp = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionAirHumidity = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionReducPression = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionWind = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionAccRain = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionVisibility = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionSolar = new System.Windows.Forms.CheckBox();
            this.checkBoxPrevisionSolarRule = new System.Windows.Forms.CheckBox();
            this.buttonGetData = new System.Windows.Forms.Button();
            this.tabPageCalc = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonSaveConfig = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonGetVariableInterval = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxConfig = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.labelVarInterval46 = new System.Windows.Forms.Label();
            this.labelVarInterval26 = new System.Windows.Forms.Label();
            this.textBoxVarInterval26 = new System.Windows.Forms.TextBox();
            this.labelVarInterval25 = new System.Windows.Forms.Label();
            this.textBoxVarInterval25 = new System.Windows.Forms.TextBox();
            this.labelVarInterval24 = new System.Windows.Forms.Label();
            this.labelVarInterval23 = new System.Windows.Forms.Label();
            this.textBoxVarInterval23 = new System.Windows.Forms.TextBox();
            this.labelVarInterval22 = new System.Windows.Forms.Label();
            this.textBoxVarInterval22 = new System.Windows.Forms.TextBox();
            this.labelVarInterval21 = new System.Windows.Forms.Label();
            this.textBoxVarInterval21 = new System.Windows.Forms.TextBox();
            this.labelVarInterval20 = new System.Windows.Forms.Label();
            this.textBoxVarInterval20 = new System.Windows.Forms.TextBox();
            this.labelVarInterval19 = new System.Windows.Forms.Label();
            this.textBoxVarInterval19 = new System.Windows.Forms.TextBox();
            this.labelVarInterval18 = new System.Windows.Forms.Label();
            this.textBoxVarInterval18 = new System.Windows.Forms.TextBox();
            this.labelVarInterval17 = new System.Windows.Forms.Label();
            this.textBoxVarInterval17 = new System.Windows.Forms.TextBox();
            this.labelVarInterval16 = new System.Windows.Forms.Label();
            this.textBoxVarInterval16 = new System.Windows.Forms.TextBox();
            this.labelVarInterval15 = new System.Windows.Forms.Label();
            this.textBoxVarInterval15 = new System.Windows.Forms.TextBox();
            this.labelVarInterval14 = new System.Windows.Forms.Label();
            this.textBoxVarInterval14 = new System.Windows.Forms.TextBox();
            this.labelVarInterval13 = new System.Windows.Forms.Label();
            this.textBoxVarInterval13 = new System.Windows.Forms.TextBox();
            this.labelVarInterval12 = new System.Windows.Forms.Label();
            this.textBoxVarInterval12 = new System.Windows.Forms.TextBox();
            this.llabelVarInterval11 = new System.Windows.Forms.Label();
            this.textBoxVarInterval11 = new System.Windows.Forms.TextBox();
            this.labelVarInterval10 = new System.Windows.Forms.Label();
            this.textBoxVarInterval10 = new System.Windows.Forms.TextBox();
            this.labelVarInterval9 = new System.Windows.Forms.Label();
            this.textBoxVarInterval9 = new System.Windows.Forms.TextBox();
            this.labelVarInterval8 = new System.Windows.Forms.Label();
            this.extBoxVarInterval58 = new System.Windows.Forms.TextBox();
            this.labelVarInterval7 = new System.Windows.Forms.Label();
            this.textBoxVarInterval7 = new System.Windows.Forms.TextBox();
            this.labelVarInterval6 = new System.Windows.Forms.Label();
            this.textBoxVarInterval6 = new System.Windows.Forms.TextBox();
            this.labelVarInterval5 = new System.Windows.Forms.Label();
            this.textBoxVarInterval5 = new System.Windows.Forms.TextBox();
            this.labelVarInterval4 = new System.Windows.Forms.Label();
            this.textBoxVarInterval4 = new System.Windows.Forms.TextBox();
            this.labelVarInterval3 = new System.Windows.Forms.Label();
            this.textBoxVarInterval3 = new System.Windows.Forms.TextBox();
            this.labelVarInterval2 = new System.Windows.Forms.Label();
            this.textBoxVarInterval2 = new System.Windows.Forms.TextBox();
            this.labelVarInterval1 = new System.Windows.Forms.Label();
            this.textBoxVarInterval1 = new System.Windows.Forms.TextBox();
            this.labelVarInterval27 = new System.Windows.Forms.Label();
            this.textBoxVarInterval27 = new System.Windows.Forms.TextBox();
            this.labelVarInterval28 = new System.Windows.Forms.Label();
            this.textBoxVarInterval28 = new System.Windows.Forms.TextBox();
            this.labelVarInterval29 = new System.Windows.Forms.Label();
            this.textBoxVarInterval29 = new System.Windows.Forms.TextBox();
            this.labelVarInterval30 = new System.Windows.Forms.Label();
            this.textBoxVarInterval30 = new System.Windows.Forms.TextBox();
            this.labelVarInterval31 = new System.Windows.Forms.Label();
            this.textBoxVarInterval31 = new System.Windows.Forms.TextBox();
            this.labelVarInterval32 = new System.Windows.Forms.Label();
            this.labelVarInterval33 = new System.Windows.Forms.Label();
            this.labelVarInterval34 = new System.Windows.Forms.Label();
            this.labelVarInterval35 = new System.Windows.Forms.Label();
            this.labelVarInterval36 = new System.Windows.Forms.Label();
            this.labelVarInterval37 = new System.Windows.Forms.Label();
            this.labelVarInterval38 = new System.Windows.Forms.Label();
            this.labelVarInterval39 = new System.Windows.Forms.Label();
            this.labelVarInterval40 = new System.Windows.Forms.Label();
            this.labelVarInterval41 = new System.Windows.Forms.Label();
            this.labelVarInterval42 = new System.Windows.Forms.Label();
            this.labelVarInterval43 = new System.Windows.Forms.Label();
            this.labelVarInterval44 = new System.Windows.Forms.Label();
            this.labelVarInterval45 = new System.Windows.Forms.Label();
            this.labelVarInterval47 = new System.Windows.Forms.Label();
            this.labelVarInterval48 = new System.Windows.Forms.Label();
            this.labelVarInterval49 = new System.Windows.Forms.Label();
            this.labelVarInterval50 = new System.Windows.Forms.Label();
            this.textBoxVarInterval32 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval33 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval34 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval35 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval36 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval37 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval38 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval39 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval40 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval41 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval42 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval43 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval44 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval45 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval46 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval47 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval48 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval49 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval50 = new System.Windows.Forms.TextBox();
            this.textBoxVarInterval24 = new System.Windows.Forms.TextBox();
            this.tabPageMaodule = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxConf3 = new System.Windows.Forms.ComboBox();
            this.comboBoxConf2 = new System.Windows.Forms.ComboBox();
            this.checkBoxConf1 = new System.Windows.Forms.CheckBox();
            this.checkBoxConf2 = new System.Windows.Forms.CheckBox();
            this.checkBoxConf3 = new System.Windows.Forms.CheckBox();
            this.comboBoxConf1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.listViewConf3 = new System.Windows.Forms.ListView();
            this.listViewConf2 = new System.Windows.Forms.ListView();
            this.listViewConf1 = new System.Windows.Forms.ListView();
            this.listViewOriginalNumbers = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.tabPageGraphics = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxGraphicConf3 = new System.Windows.Forms.ComboBox();
            this.comboBoxGraphicConf2 = new System.Windows.Forms.ComboBox();
            this.checkBoxGraphicConf1 = new System.Windows.Forms.CheckBox();
            this.checkBoxGraphicConf2 = new System.Windows.Forms.CheckBox();
            this.checkBoxGraphicConf3 = new System.Windows.Forms.CheckBox();
            this.comboBoxGraphicConf1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.radioButtonVisibility = new System.Windows.Forms.RadioButton();
            this.radioButtonReducPression = new System.Windows.Forms.RadioButton();
            this.radioButtonAirHumidity = new System.Windows.Forms.RadioButton();
            this.radioButtonThermalSensation = new System.Windows.Forms.RadioButton();
            this.radioButtonWindSpeed = new System.Windows.Forms.RadioButton();
            this.radioButtonClouds = new System.Windows.Forms.RadioButton();
            this.radioButtonRainChance = new System.Windows.Forms.RadioButton();
            this.radioButtonUVRadiation = new System.Windows.Forms.RadioButton();
            this.radioButtonTemperature = new System.Windows.Forms.RadioButton();
            this.radioButtonWind = new System.Windows.Forms.RadioButton();
            this.radioButtonAccRain = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxEnableGraphic1 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxEnableGraphic2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxEnableGraphic3 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridViewDataRead = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabControlSolarEnergy.SuspendLayout();
            this.tabPageData.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabPageCalc.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tableLayoutPanel22.SuspendLayout();
            this.tabPageMaodule.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tabPageGraphics.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataRead)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1379, 610);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePickerEndTime, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePickerEndDate, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePickerStartTime, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePickerStartDate, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.buttonSetData, 4, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1373, 59);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1196, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Confirmar Data";
            // 
            // dateTimePickerEndTime
            // 
            this.dateTimePickerEndTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerEndTime.Location = new System.Drawing.Point(825, 34);
            this.dateTimePickerEndTime.Name = "dateTimePickerEndTime";
            this.dateTimePickerEndTime.Size = new System.Drawing.Size(268, 20);
            this.dateTimePickerEndTime.TabIndex = 7;
            this.dateTimePickerEndTime.Value = new System.DateTime(2018, 9, 25, 23, 59, 59, 0);
            // 
            // dateTimePickerEndDate
            // 
            this.dateTimePickerEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerEndDate.Location = new System.Drawing.Point(551, 34);
            this.dateTimePickerEndDate.Name = "dateTimePickerEndDate";
            this.dateTimePickerEndDate.Size = new System.Drawing.Size(268, 20);
            this.dateTimePickerEndDate.TabIndex = 6;
            this.dateTimePickerEndDate.Value = new System.DateTime(2018, 8, 23, 0, 0, 0, 0);
            // 
            // dateTimePickerStartTime
            // 
            this.dateTimePickerStartTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerStartTime.Location = new System.Drawing.Point(277, 34);
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.Size = new System.Drawing.Size(268, 20);
            this.dateTimePickerStartTime.TabIndex = 5;
            this.dateTimePickerStartTime.Value = new System.DateTime(2018, 9, 25, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(926, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Horario Final";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(657, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Final";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(375, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Horario Inicial";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Inicial";
            // 
            // dateTimePickerStartDate
            // 
            this.dateTimePickerStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerStartDate.Location = new System.Drawing.Point(3, 34);
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            this.dateTimePickerStartDate.Size = new System.Drawing.Size(268, 20);
            this.dateTimePickerStartDate.TabIndex = 4;
            this.dateTimePickerStartDate.Value = new System.DateTime(2018, 8, 22, 0, 0, 0, 0);
            // 
            // buttonSetData
            // 
            this.buttonSetData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSetData.Location = new System.Drawing.Point(1099, 32);
            this.buttonSetData.Name = "buttonSetData";
            this.buttonSetData.Size = new System.Drawing.Size(271, 24);
            this.buttonSetData.TabIndex = 8;
            this.buttonSetData.Text = "OK";
            this.buttonSetData.UseVisualStyleBackColor = true;
            this.buttonSetData.Click += new System.EventHandler(this.buttonSetData_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tabControlSolarEnergy, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 68);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1373, 539);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tabControlSolarEnergy
            // 
            this.tabControlSolarEnergy.Controls.Add(this.tabPageData);
            this.tabControlSolarEnergy.Controls.Add(this.tabPageCalc);
            this.tabControlSolarEnergy.Controls.Add(this.tabPageMaodule);
            this.tabControlSolarEnergy.Controls.Add(this.tabPageGraphics);
            this.tabControlSolarEnergy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSolarEnergy.Location = new System.Drawing.Point(3, 3);
            this.tabControlSolarEnergy.Name = "tabControlSolarEnergy";
            this.tabControlSolarEnergy.SelectedIndex = 0;
            this.tabControlSolarEnergy.Size = new System.Drawing.Size(1367, 533);
            this.tabControlSolarEnergy.TabIndex = 0;
            // 
            // tabPageData
            // 
            this.tabPageData.Controls.Add(this.tableLayoutPanel7);
            this.tabPageData.Location = new System.Drawing.Point(4, 22);
            this.tabPageData.Name = "tabPageData";
            this.tabPageData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageData.Size = new System.Drawing.Size(1359, 507);
            this.tabPageData.TabIndex = 0;
            this.tabPageData.Text = "Dados";
            this.tabPageData.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1353, 501);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.dataGridViewDataRead, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1347, 495);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 8;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250275F));
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionRainChance, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionClouds, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionWindSpeed, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionUVRadiation, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxDataRadiation, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxDataPot, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionThermalSensation, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionTemp, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionAirHumidity, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionReducPression, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionWind, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionAccRain, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionVisibility, 4, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionSolar, 5, 1);
            this.tableLayoutPanel9.Controls.Add(this.checkBoxPrevisionSolarRule, 6, 1);
            this.tableLayoutPanel9.Controls.Add(this.buttonGetData, 7, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1341, 89);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // checkBoxPrevisionRainChance
            // 
            this.checkBoxPrevisionRainChance.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionRainChance.AutoSize = true;
            this.checkBoxPrevisionRainChance.Location = new System.Drawing.Point(671, 13);
            this.checkBoxPrevisionRainChance.Name = "checkBoxPrevisionRainChance";
            this.checkBoxPrevisionRainChance.Size = new System.Drawing.Size(83, 17);
            this.checkBoxPrevisionRainChance.TabIndex = 12;
            this.checkBoxPrevisionRainChance.Text = "Preciptacao";
            this.checkBoxPrevisionRainChance.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionClouds
            // 
            this.checkBoxPrevisionClouds.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionClouds.AutoSize = true;
            this.checkBoxPrevisionClouds.Location = new System.Drawing.Point(838, 13);
            this.checkBoxPrevisionClouds.Name = "checkBoxPrevisionClouds";
            this.checkBoxPrevisionClouds.Size = new System.Drawing.Size(63, 17);
            this.checkBoxPrevisionClouds.TabIndex = 11;
            this.checkBoxPrevisionClouds.Text = "Nuvens";
            this.checkBoxPrevisionClouds.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionWindSpeed
            // 
            this.checkBoxPrevisionWindSpeed.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionWindSpeed.AutoSize = true;
            this.checkBoxPrevisionWindSpeed.Location = new System.Drawing.Point(1005, 13);
            this.checkBoxPrevisionWindSpeed.Name = "checkBoxPrevisionWindSpeed";
            this.checkBoxPrevisionWindSpeed.Size = new System.Drawing.Size(125, 17);
            this.checkBoxPrevisionWindSpeed.TabIndex = 10;
            this.checkBoxPrevisionWindSpeed.Text = "Velocidade do Vento";
            this.checkBoxPrevisionWindSpeed.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionUVRadiation
            // 
            this.checkBoxPrevisionUVRadiation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionUVRadiation.AutoSize = true;
            this.checkBoxPrevisionUVRadiation.Location = new System.Drawing.Point(504, 13);
            this.checkBoxPrevisionUVRadiation.Name = "checkBoxPrevisionUVRadiation";
            this.checkBoxPrevisionUVRadiation.Size = new System.Drawing.Size(73, 17);
            this.checkBoxPrevisionUVRadiation.TabIndex = 9;
            this.checkBoxPrevisionUVRadiation.Text = "Indice UV";
            this.checkBoxPrevisionUVRadiation.UseVisualStyleBackColor = true;
            // 
            // checkBoxDataRadiation
            // 
            this.checkBoxDataRadiation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxDataRadiation.AutoSize = true;
            this.checkBoxDataRadiation.Location = new System.Drawing.Point(3, 13);
            this.checkBoxDataRadiation.Name = "checkBoxDataRadiation";
            this.checkBoxDataRadiation.Size = new System.Drawing.Size(73, 17);
            this.checkBoxDataRadiation.TabIndex = 8;
            this.checkBoxDataRadiation.Text = "Irradiacao";
            this.checkBoxDataRadiation.UseVisualStyleBackColor = true;
            // 
            // checkBoxDataPot
            // 
            this.checkBoxDataPot.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxDataPot.AutoSize = true;
            this.checkBoxDataPot.Location = new System.Drawing.Point(170, 13);
            this.checkBoxDataPot.Name = "checkBoxDataPot";
            this.checkBoxDataPot.Size = new System.Drawing.Size(68, 17);
            this.checkBoxDataPot.TabIndex = 7;
            this.checkBoxDataPot.Text = "Potencia";
            this.checkBoxDataPot.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionThermalSensation
            // 
            this.checkBoxPrevisionThermalSensation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionThermalSensation.AutoSize = true;
            this.checkBoxPrevisionThermalSensation.Location = new System.Drawing.Point(1172, 13);
            this.checkBoxPrevisionThermalSensation.Name = "checkBoxPrevisionThermalSensation";
            this.checkBoxPrevisionThermalSensation.Size = new System.Drawing.Size(115, 17);
            this.checkBoxPrevisionThermalSensation.TabIndex = 1;
            this.checkBoxPrevisionThermalSensation.Text = "Sensacao Termica";
            this.checkBoxPrevisionThermalSensation.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionTemp
            // 
            this.checkBoxPrevisionTemp.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionTemp.AutoSize = true;
            this.checkBoxPrevisionTemp.Location = new System.Drawing.Point(337, 13);
            this.checkBoxPrevisionTemp.Name = "checkBoxPrevisionTemp";
            this.checkBoxPrevisionTemp.Size = new System.Drawing.Size(86, 17);
            this.checkBoxPrevisionTemp.TabIndex = 0;
            this.checkBoxPrevisionTemp.Text = "Temperatura";
            this.checkBoxPrevisionTemp.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionAirHumidity
            // 
            this.checkBoxPrevisionAirHumidity.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionAirHumidity.AutoSize = true;
            this.checkBoxPrevisionAirHumidity.Location = new System.Drawing.Point(3, 58);
            this.checkBoxPrevisionAirHumidity.Name = "checkBoxPrevisionAirHumidity";
            this.checkBoxPrevisionAirHumidity.Size = new System.Drawing.Size(102, 17);
            this.checkBoxPrevisionAirHumidity.TabIndex = 2;
            this.checkBoxPrevisionAirHumidity.Text = "Humidade do Ar";
            this.checkBoxPrevisionAirHumidity.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionReducPression
            // 
            this.checkBoxPrevisionReducPression.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionReducPression.AutoSize = true;
            this.checkBoxPrevisionReducPression.Location = new System.Drawing.Point(170, 58);
            this.checkBoxPrevisionReducPression.Name = "checkBoxPrevisionReducPression";
            this.checkBoxPrevisionReducPression.Size = new System.Drawing.Size(112, 17);
            this.checkBoxPrevisionReducPression.TabIndex = 3;
            this.checkBoxPrevisionReducPression.Text = "Pressao Reduzida";
            this.checkBoxPrevisionReducPression.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionWind
            // 
            this.checkBoxPrevisionWind.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionWind.AutoSize = true;
            this.checkBoxPrevisionWind.Location = new System.Drawing.Point(337, 58);
            this.checkBoxPrevisionWind.Name = "checkBoxPrevisionWind";
            this.checkBoxPrevisionWind.Size = new System.Drawing.Size(60, 17);
            this.checkBoxPrevisionWind.TabIndex = 4;
            this.checkBoxPrevisionWind.Text = "Rajada";
            this.checkBoxPrevisionWind.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionAccRain
            // 
            this.checkBoxPrevisionAccRain.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionAccRain.AutoSize = true;
            this.checkBoxPrevisionAccRain.Location = new System.Drawing.Point(504, 58);
            this.checkBoxPrevisionAccRain.Name = "checkBoxPrevisionAccRain";
            this.checkBoxPrevisionAccRain.Size = new System.Drawing.Size(139, 17);
            this.checkBoxPrevisionAccRain.TabIndex = 14;
            this.checkBoxPrevisionAccRain.Text = "Preciptacao Acumulada";
            this.checkBoxPrevisionAccRain.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionVisibility
            // 
            this.checkBoxPrevisionVisibility.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionVisibility.AutoSize = true;
            this.checkBoxPrevisionVisibility.Location = new System.Drawing.Point(671, 58);
            this.checkBoxPrevisionVisibility.Name = "checkBoxPrevisionVisibility";
            this.checkBoxPrevisionVisibility.Size = new System.Drawing.Size(78, 17);
            this.checkBoxPrevisionVisibility.TabIndex = 13;
            this.checkBoxPrevisionVisibility.Text = "Visibilidade";
            this.checkBoxPrevisionVisibility.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionSolar
            // 
            this.checkBoxPrevisionSolar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionSolar.AutoSize = true;
            this.checkBoxPrevisionSolar.Location = new System.Drawing.Point(838, 58);
            this.checkBoxPrevisionSolar.Name = "checkBoxPrevisionSolar";
            this.checkBoxPrevisionSolar.Size = new System.Drawing.Size(41, 17);
            this.checkBoxPrevisionSolar.TabIndex = 5;
            this.checkBoxPrevisionSolar.Text = "Sol";
            this.checkBoxPrevisionSolar.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrevisionSolarRule
            // 
            this.checkBoxPrevisionSolarRule.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.checkBoxPrevisionSolarRule.AutoSize = true;
            this.checkBoxPrevisionSolarRule.Location = new System.Drawing.Point(1005, 58);
            this.checkBoxPrevisionSolarRule.Name = "checkBoxPrevisionSolarRule";
            this.checkBoxPrevisionSolarRule.Size = new System.Drawing.Size(73, 17);
            this.checkBoxPrevisionSolarRule.TabIndex = 6;
            this.checkBoxPrevisionSolarRule.Text = "Sol Regra";
            this.checkBoxPrevisionSolarRule.UseVisualStyleBackColor = true;
            // 
            // buttonGetData
            // 
            this.buttonGetData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGetData.Location = new System.Drawing.Point(1172, 55);
            this.buttonGetData.Name = "buttonGetData";
            this.buttonGetData.Size = new System.Drawing.Size(166, 23);
            this.buttonGetData.TabIndex = 15;
            this.buttonGetData.Text = "BUSCAR";
            this.buttonGetData.UseVisualStyleBackColor = true;
            this.buttonGetData.Click += new System.EventHandler(this.buttonGetData_Click);
            // 
            // tabPageCalc
            // 
            this.tabPageCalc.Controls.Add(this.tableLayoutPanel10);
            this.tabPageCalc.Location = new System.Drawing.Point(4, 22);
            this.tabPageCalc.Name = "tabPageCalc";
            this.tabPageCalc.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCalc.Size = new System.Drawing.Size(1359, 507);
            this.tabPageCalc.TabIndex = 1;
            this.tabPageCalc.Text = "Calculos";
            this.tabPageCalc.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1353, 501);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5464F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.4536F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel21, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel22, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1347, 495);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.buttonSaveConfig, 0, 7);
            this.tableLayoutPanel21.Controls.Add(this.comboBox1, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel21.Controls.Add(this.buttonGetVariableInterval, 0, 4);
            this.tableLayoutPanel21.Controls.Add(this.numericUpDown1, 0, 3);
            this.tableLayoutPanel21.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel21.Controls.Add(this.comboBoxConfig, 0, 6);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 8;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(163, 270);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // buttonSaveConfig
            // 
            this.buttonSaveConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveConfig.Location = new System.Drawing.Point(3, 236);
            this.buttonSaveConfig.Name = "buttonSaveConfig";
            this.buttonSaveConfig.Size = new System.Drawing.Size(157, 25);
            this.buttonSaveConfig.TabIndex = 7;
            this.buttonSaveConfig.Text = "Definir";
            this.buttonSaveConfig.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 30);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(157, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Variavel";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(157, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Intervalo";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonGetVariableInterval
            // 
            this.buttonGetVariableInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGetVariableInterval.Location = new System.Drawing.Point(3, 132);
            this.buttonGetVariableInterval.Name = "buttonGetVariableInterval";
            this.buttonGetVariableInterval.Size = new System.Drawing.Size(157, 25);
            this.buttonGetVariableInterval.TabIndex = 2;
            this.buttonGetVariableInterval.Text = "Definir";
            this.buttonGetVariableInterval.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(3, 93);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown1.TabIndex = 4;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Intervalo";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxConfig
            // 
            this.comboBoxConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxConfig.FormattingEnabled = true;
            this.comboBoxConfig.Location = new System.Drawing.Point(3, 196);
            this.comboBoxConfig.Name = "comboBoxConfig";
            this.comboBoxConfig.Size = new System.Drawing.Size(157, 21);
            this.comboBoxConfig.TabIndex = 6;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 10;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval46, 0, 9);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval26, 0, 5);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval26, 1, 5);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval25, 8, 4);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval25, 9, 4);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval24, 6, 4);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval23, 4, 4);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval23, 5, 4);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval22, 2, 4);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval22, 3, 4);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval21, 0, 4);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval21, 1, 4);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval20, 8, 3);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval20, 9, 3);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval19, 6, 3);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval19, 7, 3);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval18, 4, 3);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval18, 5, 3);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval17, 2, 3);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval17, 3, 3);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval16, 0, 3);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval16, 1, 3);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval15, 8, 2);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval15, 9, 2);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval14, 6, 2);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval14, 7, 2);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval13, 4, 2);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval13, 5, 2);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval12, 2, 2);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval12, 3, 2);
            this.tableLayoutPanel22.Controls.Add(this.llabelVarInterval11, 0, 2);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval11, 1, 2);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval10, 8, 1);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval10, 9, 1);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval9, 6, 1);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval9, 7, 1);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval8, 4, 1);
            this.tableLayoutPanel22.Controls.Add(this.extBoxVarInterval58, 5, 1);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval7, 2, 1);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval7, 3, 1);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval6, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval6, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval5, 8, 0);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval5, 9, 0);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval4, 6, 0);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval4, 7, 0);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval3, 4, 0);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval3, 5, 0);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval2, 2, 0);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval2, 3, 0);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval1, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval1, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval27, 2, 5);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval27, 3, 5);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval28, 4, 5);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval28, 5, 5);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval29, 6, 5);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval29, 7, 5);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval30, 8, 5);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval30, 9, 5);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval31, 0, 6);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval31, 1, 6);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval32, 2, 6);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval33, 4, 6);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval34, 6, 6);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval35, 8, 6);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval36, 0, 7);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval37, 2, 7);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval38, 4, 7);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval39, 6, 7);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval40, 8, 7);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval41, 0, 8);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval42, 2, 8);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval43, 4, 8);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval44, 6, 8);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval45, 8, 8);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval47, 2, 9);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval48, 4, 9);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval49, 6, 9);
            this.tableLayoutPanel22.Controls.Add(this.labelVarInterval50, 8, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval32, 3, 6);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval33, 5, 6);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval34, 7, 6);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval35, 9, 6);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval36, 1, 7);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval37, 3, 7);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval38, 5, 7);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval39, 7, 7);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval40, 9, 7);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval41, 1, 8);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval42, 3, 8);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval43, 5, 8);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval44, 7, 8);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval45, 9, 8);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval46, 1, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval47, 3, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval48, 5, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval49, 7, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval50, 9, 9);
            this.tableLayoutPanel22.Controls.Add(this.textBoxVarInterval24, 7, 4);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(172, 3);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 10;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1172, 489);
            this.tableLayoutPanel22.TabIndex = 1;
            // 
            // labelVarInterval46
            // 
            this.labelVarInterval46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval46.AutoSize = true;
            this.labelVarInterval46.Location = new System.Drawing.Point(3, 454);
            this.labelVarInterval46.Name = "labelVarInterval46";
            this.labelVarInterval46.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval46.TabIndex = 95;
            this.labelVarInterval46.Text = "label56";
            this.labelVarInterval46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval26
            // 
            this.labelVarInterval26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval26.AutoSize = true;
            this.labelVarInterval26.Location = new System.Drawing.Point(3, 257);
            this.labelVarInterval26.Name = "labelVarInterval26";
            this.labelVarInterval26.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval26.TabIndex = 75;
            this.labelVarInterval26.Text = "label36";
            this.labelVarInterval26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval26
            // 
            this.textBoxVarInterval26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval26.Location = new System.Drawing.Point(120, 254);
            this.textBoxVarInterval26.Name = "textBoxVarInterval26";
            this.textBoxVarInterval26.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval26.TabIndex = 25;
            // 
            // labelVarInterval25
            // 
            this.labelVarInterval25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval25.AutoSize = true;
            this.labelVarInterval25.Location = new System.Drawing.Point(939, 209);
            this.labelVarInterval25.Name = "labelVarInterval25";
            this.labelVarInterval25.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval25.TabIndex = 74;
            this.labelVarInterval25.Text = "label35";
            this.labelVarInterval25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval25
            // 
            this.textBoxVarInterval25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval25.Location = new System.Drawing.Point(1056, 206);
            this.textBoxVarInterval25.Name = "textBoxVarInterval25";
            this.textBoxVarInterval25.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval25.TabIndex = 24;
            // 
            // labelVarInterval24
            // 
            this.labelVarInterval24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval24.AutoSize = true;
            this.labelVarInterval24.Location = new System.Drawing.Point(705, 209);
            this.labelVarInterval24.Name = "labelVarInterval24";
            this.labelVarInterval24.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval24.TabIndex = 73;
            this.labelVarInterval24.Text = "label34";
            this.labelVarInterval24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval23
            // 
            this.labelVarInterval23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval23.AutoSize = true;
            this.labelVarInterval23.Location = new System.Drawing.Point(471, 209);
            this.labelVarInterval23.Name = "labelVarInterval23";
            this.labelVarInterval23.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval23.TabIndex = 72;
            this.labelVarInterval23.Text = "label33";
            this.labelVarInterval23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval23
            // 
            this.textBoxVarInterval23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval23.Location = new System.Drawing.Point(588, 206);
            this.textBoxVarInterval23.Name = "textBoxVarInterval23";
            this.textBoxVarInterval23.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval23.TabIndex = 22;
            // 
            // labelVarInterval22
            // 
            this.labelVarInterval22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval22.AutoSize = true;
            this.labelVarInterval22.Location = new System.Drawing.Point(237, 209);
            this.labelVarInterval22.Name = "labelVarInterval22";
            this.labelVarInterval22.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval22.TabIndex = 71;
            this.labelVarInterval22.Text = "label32";
            this.labelVarInterval22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval22
            // 
            this.textBoxVarInterval22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval22.Location = new System.Drawing.Point(354, 206);
            this.textBoxVarInterval22.Name = "textBoxVarInterval22";
            this.textBoxVarInterval22.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval22.TabIndex = 21;
            // 
            // labelVarInterval21
            // 
            this.labelVarInterval21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval21.AutoSize = true;
            this.labelVarInterval21.Location = new System.Drawing.Point(3, 209);
            this.labelVarInterval21.Name = "labelVarInterval21";
            this.labelVarInterval21.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval21.TabIndex = 70;
            this.labelVarInterval21.Text = "label31";
            this.labelVarInterval21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval21
            // 
            this.textBoxVarInterval21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval21.Location = new System.Drawing.Point(120, 206);
            this.textBoxVarInterval21.Name = "textBoxVarInterval21";
            this.textBoxVarInterval21.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval21.TabIndex = 20;
            // 
            // labelVarInterval20
            // 
            this.labelVarInterval20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval20.AutoSize = true;
            this.labelVarInterval20.Location = new System.Drawing.Point(939, 161);
            this.labelVarInterval20.Name = "labelVarInterval20";
            this.labelVarInterval20.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval20.TabIndex = 69;
            this.labelVarInterval20.Text = "label30";
            this.labelVarInterval20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval20
            // 
            this.textBoxVarInterval20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval20.Location = new System.Drawing.Point(1056, 158);
            this.textBoxVarInterval20.Name = "textBoxVarInterval20";
            this.textBoxVarInterval20.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval20.TabIndex = 19;
            // 
            // labelVarInterval19
            // 
            this.labelVarInterval19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval19.AutoSize = true;
            this.labelVarInterval19.Location = new System.Drawing.Point(705, 161);
            this.labelVarInterval19.Name = "labelVarInterval19";
            this.labelVarInterval19.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval19.TabIndex = 68;
            this.labelVarInterval19.Text = "label29";
            this.labelVarInterval19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval19
            // 
            this.textBoxVarInterval19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval19.Location = new System.Drawing.Point(822, 158);
            this.textBoxVarInterval19.Name = "textBoxVarInterval19";
            this.textBoxVarInterval19.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval19.TabIndex = 18;
            // 
            // labelVarInterval18
            // 
            this.labelVarInterval18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval18.AutoSize = true;
            this.labelVarInterval18.Location = new System.Drawing.Point(471, 161);
            this.labelVarInterval18.Name = "labelVarInterval18";
            this.labelVarInterval18.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval18.TabIndex = 67;
            this.labelVarInterval18.Text = "label28";
            this.labelVarInterval18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval18
            // 
            this.textBoxVarInterval18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval18.Location = new System.Drawing.Point(588, 158);
            this.textBoxVarInterval18.Name = "textBoxVarInterval18";
            this.textBoxVarInterval18.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval18.TabIndex = 17;
            // 
            // labelVarInterval17
            // 
            this.labelVarInterval17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval17.AutoSize = true;
            this.labelVarInterval17.Location = new System.Drawing.Point(237, 161);
            this.labelVarInterval17.Name = "labelVarInterval17";
            this.labelVarInterval17.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval17.TabIndex = 66;
            this.labelVarInterval17.Text = "label27";
            this.labelVarInterval17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval17
            // 
            this.textBoxVarInterval17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval17.Location = new System.Drawing.Point(354, 158);
            this.textBoxVarInterval17.Name = "textBoxVarInterval17";
            this.textBoxVarInterval17.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval17.TabIndex = 16;
            // 
            // labelVarInterval16
            // 
            this.labelVarInterval16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval16.AutoSize = true;
            this.labelVarInterval16.Location = new System.Drawing.Point(3, 161);
            this.labelVarInterval16.Name = "labelVarInterval16";
            this.labelVarInterval16.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval16.TabIndex = 65;
            this.labelVarInterval16.Text = "label26";
            this.labelVarInterval16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval16
            // 
            this.textBoxVarInterval16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval16.Location = new System.Drawing.Point(120, 158);
            this.textBoxVarInterval16.Name = "textBoxVarInterval16";
            this.textBoxVarInterval16.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval16.TabIndex = 15;
            // 
            // labelVarInterval15
            // 
            this.labelVarInterval15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval15.AutoSize = true;
            this.labelVarInterval15.Location = new System.Drawing.Point(939, 113);
            this.labelVarInterval15.Name = "labelVarInterval15";
            this.labelVarInterval15.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval15.TabIndex = 64;
            this.labelVarInterval15.Text = "label25";
            this.labelVarInterval15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval15
            // 
            this.textBoxVarInterval15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval15.Location = new System.Drawing.Point(1056, 110);
            this.textBoxVarInterval15.Name = "textBoxVarInterval15";
            this.textBoxVarInterval15.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval15.TabIndex = 14;
            // 
            // labelVarInterval14
            // 
            this.labelVarInterval14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval14.AutoSize = true;
            this.labelVarInterval14.Location = new System.Drawing.Point(705, 113);
            this.labelVarInterval14.Name = "labelVarInterval14";
            this.labelVarInterval14.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval14.TabIndex = 63;
            this.labelVarInterval14.Text = "label24";
            this.labelVarInterval14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval14
            // 
            this.textBoxVarInterval14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval14.Location = new System.Drawing.Point(822, 110);
            this.textBoxVarInterval14.Name = "textBoxVarInterval14";
            this.textBoxVarInterval14.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval14.TabIndex = 13;
            // 
            // labelVarInterval13
            // 
            this.labelVarInterval13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval13.AutoSize = true;
            this.labelVarInterval13.Location = new System.Drawing.Point(471, 113);
            this.labelVarInterval13.Name = "labelVarInterval13";
            this.labelVarInterval13.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval13.TabIndex = 62;
            this.labelVarInterval13.Text = "label23";
            this.labelVarInterval13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval13
            // 
            this.textBoxVarInterval13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval13.Location = new System.Drawing.Point(588, 110);
            this.textBoxVarInterval13.Name = "textBoxVarInterval13";
            this.textBoxVarInterval13.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval13.TabIndex = 12;
            // 
            // labelVarInterval12
            // 
            this.labelVarInterval12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval12.AutoSize = true;
            this.labelVarInterval12.Location = new System.Drawing.Point(237, 113);
            this.labelVarInterval12.Name = "labelVarInterval12";
            this.labelVarInterval12.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval12.TabIndex = 61;
            this.labelVarInterval12.Text = "label22";
            this.labelVarInterval12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval12
            // 
            this.textBoxVarInterval12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval12.Location = new System.Drawing.Point(354, 110);
            this.textBoxVarInterval12.Name = "textBoxVarInterval12";
            this.textBoxVarInterval12.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval12.TabIndex = 11;
            // 
            // llabelVarInterval11
            // 
            this.llabelVarInterval11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.llabelVarInterval11.AutoSize = true;
            this.llabelVarInterval11.Location = new System.Drawing.Point(3, 113);
            this.llabelVarInterval11.Name = "llabelVarInterval11";
            this.llabelVarInterval11.Size = new System.Drawing.Size(111, 13);
            this.llabelVarInterval11.TabIndex = 60;
            this.llabelVarInterval11.Text = "label21";
            this.llabelVarInterval11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval11
            // 
            this.textBoxVarInterval11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval11.Location = new System.Drawing.Point(120, 110);
            this.textBoxVarInterval11.Name = "textBoxVarInterval11";
            this.textBoxVarInterval11.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval11.TabIndex = 10;
            // 
            // labelVarInterval10
            // 
            this.labelVarInterval10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval10.AutoSize = true;
            this.labelVarInterval10.Location = new System.Drawing.Point(939, 65);
            this.labelVarInterval10.Name = "labelVarInterval10";
            this.labelVarInterval10.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval10.TabIndex = 59;
            this.labelVarInterval10.Text = "label20";
            this.labelVarInterval10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval10
            // 
            this.textBoxVarInterval10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval10.Location = new System.Drawing.Point(1056, 62);
            this.textBoxVarInterval10.Name = "textBoxVarInterval10";
            this.textBoxVarInterval10.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval10.TabIndex = 9;
            // 
            // labelVarInterval9
            // 
            this.labelVarInterval9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval9.AutoSize = true;
            this.labelVarInterval9.Location = new System.Drawing.Point(705, 65);
            this.labelVarInterval9.Name = "labelVarInterval9";
            this.labelVarInterval9.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval9.TabIndex = 58;
            this.labelVarInterval9.Text = "label19";
            this.labelVarInterval9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval9
            // 
            this.textBoxVarInterval9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval9.Location = new System.Drawing.Point(822, 62);
            this.textBoxVarInterval9.Name = "textBoxVarInterval9";
            this.textBoxVarInterval9.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval9.TabIndex = 8;
            // 
            // labelVarInterval8
            // 
            this.labelVarInterval8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval8.AutoSize = true;
            this.labelVarInterval8.Location = new System.Drawing.Point(471, 65);
            this.labelVarInterval8.Name = "labelVarInterval8";
            this.labelVarInterval8.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval8.TabIndex = 57;
            this.labelVarInterval8.Text = "label18";
            this.labelVarInterval8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // extBoxVarInterval58
            // 
            this.extBoxVarInterval58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.extBoxVarInterval58.Location = new System.Drawing.Point(588, 62);
            this.extBoxVarInterval58.Name = "extBoxVarInterval58";
            this.extBoxVarInterval58.Size = new System.Drawing.Size(111, 20);
            this.extBoxVarInterval58.TabIndex = 7;
            // 
            // labelVarInterval7
            // 
            this.labelVarInterval7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval7.AutoSize = true;
            this.labelVarInterval7.Location = new System.Drawing.Point(237, 65);
            this.labelVarInterval7.Name = "labelVarInterval7";
            this.labelVarInterval7.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval7.TabIndex = 56;
            this.labelVarInterval7.Text = "label17";
            this.labelVarInterval7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval7
            // 
            this.textBoxVarInterval7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval7.Location = new System.Drawing.Point(354, 62);
            this.textBoxVarInterval7.Name = "textBoxVarInterval7";
            this.textBoxVarInterval7.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval7.TabIndex = 6;
            // 
            // labelVarInterval6
            // 
            this.labelVarInterval6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval6.AutoSize = true;
            this.labelVarInterval6.Location = new System.Drawing.Point(3, 65);
            this.labelVarInterval6.Name = "labelVarInterval6";
            this.labelVarInterval6.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval6.TabIndex = 55;
            this.labelVarInterval6.Text = "label16";
            this.labelVarInterval6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval6
            // 
            this.textBoxVarInterval6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval6.Location = new System.Drawing.Point(120, 62);
            this.textBoxVarInterval6.Name = "textBoxVarInterval6";
            this.textBoxVarInterval6.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval6.TabIndex = 5;
            // 
            // labelVarInterval5
            // 
            this.labelVarInterval5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval5.AutoSize = true;
            this.labelVarInterval5.Location = new System.Drawing.Point(939, 17);
            this.labelVarInterval5.Name = "labelVarInterval5";
            this.labelVarInterval5.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval5.TabIndex = 54;
            this.labelVarInterval5.Text = "label15";
            this.labelVarInterval5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval5
            // 
            this.textBoxVarInterval5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval5.Location = new System.Drawing.Point(1056, 14);
            this.textBoxVarInterval5.Name = "textBoxVarInterval5";
            this.textBoxVarInterval5.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval5.TabIndex = 4;
            // 
            // labelVarInterval4
            // 
            this.labelVarInterval4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval4.AutoSize = true;
            this.labelVarInterval4.Location = new System.Drawing.Point(705, 17);
            this.labelVarInterval4.Name = "labelVarInterval4";
            this.labelVarInterval4.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval4.TabIndex = 53;
            this.labelVarInterval4.Text = "label14";
            this.labelVarInterval4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval4
            // 
            this.textBoxVarInterval4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval4.Location = new System.Drawing.Point(822, 14);
            this.textBoxVarInterval4.Name = "textBoxVarInterval4";
            this.textBoxVarInterval4.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval4.TabIndex = 3;
            // 
            // labelVarInterval3
            // 
            this.labelVarInterval3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval3.AutoSize = true;
            this.labelVarInterval3.Location = new System.Drawing.Point(471, 17);
            this.labelVarInterval3.Name = "labelVarInterval3";
            this.labelVarInterval3.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval3.TabIndex = 52;
            this.labelVarInterval3.Text = "label13";
            this.labelVarInterval3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval3
            // 
            this.textBoxVarInterval3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval3.Location = new System.Drawing.Point(588, 14);
            this.textBoxVarInterval3.Name = "textBoxVarInterval3";
            this.textBoxVarInterval3.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval3.TabIndex = 2;
            // 
            // labelVarInterval2
            // 
            this.labelVarInterval2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval2.AutoSize = true;
            this.labelVarInterval2.Location = new System.Drawing.Point(237, 17);
            this.labelVarInterval2.Name = "labelVarInterval2";
            this.labelVarInterval2.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval2.TabIndex = 51;
            this.labelVarInterval2.Text = "label12";
            this.labelVarInterval2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval2
            // 
            this.textBoxVarInterval2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval2.Location = new System.Drawing.Point(354, 14);
            this.textBoxVarInterval2.Name = "textBoxVarInterval2";
            this.textBoxVarInterval2.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval2.TabIndex = 1;
            // 
            // labelVarInterval1
            // 
            this.labelVarInterval1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval1.AutoSize = true;
            this.labelVarInterval1.Location = new System.Drawing.Point(3, 17);
            this.labelVarInterval1.Name = "labelVarInterval1";
            this.labelVarInterval1.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval1.TabIndex = 50;
            this.labelVarInterval1.Text = "label11";
            this.labelVarInterval1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval1
            // 
            this.textBoxVarInterval1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval1.Location = new System.Drawing.Point(120, 14);
            this.textBoxVarInterval1.Name = "textBoxVarInterval1";
            this.textBoxVarInterval1.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval1.TabIndex = 0;
            // 
            // labelVarInterval27
            // 
            this.labelVarInterval27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval27.AutoSize = true;
            this.labelVarInterval27.Location = new System.Drawing.Point(237, 257);
            this.labelVarInterval27.Name = "labelVarInterval27";
            this.labelVarInterval27.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval27.TabIndex = 76;
            this.labelVarInterval27.Text = "label37";
            this.labelVarInterval27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval27
            // 
            this.textBoxVarInterval27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval27.Location = new System.Drawing.Point(354, 254);
            this.textBoxVarInterval27.Name = "textBoxVarInterval27";
            this.textBoxVarInterval27.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval27.TabIndex = 26;
            // 
            // labelVarInterval28
            // 
            this.labelVarInterval28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval28.AutoSize = true;
            this.labelVarInterval28.Location = new System.Drawing.Point(471, 257);
            this.labelVarInterval28.Name = "labelVarInterval28";
            this.labelVarInterval28.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval28.TabIndex = 77;
            this.labelVarInterval28.Text = "label38";
            this.labelVarInterval28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval28
            // 
            this.textBoxVarInterval28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval28.Location = new System.Drawing.Point(588, 254);
            this.textBoxVarInterval28.Name = "textBoxVarInterval28";
            this.textBoxVarInterval28.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval28.TabIndex = 27;
            // 
            // labelVarInterval29
            // 
            this.labelVarInterval29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval29.AutoSize = true;
            this.labelVarInterval29.Location = new System.Drawing.Point(705, 257);
            this.labelVarInterval29.Name = "labelVarInterval29";
            this.labelVarInterval29.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval29.TabIndex = 78;
            this.labelVarInterval29.Text = "label39";
            this.labelVarInterval29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval29
            // 
            this.textBoxVarInterval29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval29.Location = new System.Drawing.Point(822, 254);
            this.textBoxVarInterval29.Name = "textBoxVarInterval29";
            this.textBoxVarInterval29.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval29.TabIndex = 28;
            // 
            // labelVarInterval30
            // 
            this.labelVarInterval30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval30.AutoSize = true;
            this.labelVarInterval30.Location = new System.Drawing.Point(939, 257);
            this.labelVarInterval30.Name = "labelVarInterval30";
            this.labelVarInterval30.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval30.TabIndex = 79;
            this.labelVarInterval30.Text = "label40";
            this.labelVarInterval30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval30
            // 
            this.textBoxVarInterval30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval30.Location = new System.Drawing.Point(1056, 254);
            this.textBoxVarInterval30.Name = "textBoxVarInterval30";
            this.textBoxVarInterval30.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval30.TabIndex = 29;
            // 
            // labelVarInterval31
            // 
            this.labelVarInterval31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval31.AutoSize = true;
            this.labelVarInterval31.Location = new System.Drawing.Point(3, 305);
            this.labelVarInterval31.Name = "labelVarInterval31";
            this.labelVarInterval31.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval31.TabIndex = 80;
            this.labelVarInterval31.Text = "label41";
            this.labelVarInterval31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval31
            // 
            this.textBoxVarInterval31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval31.Location = new System.Drawing.Point(120, 302);
            this.textBoxVarInterval31.Name = "textBoxVarInterval31";
            this.textBoxVarInterval31.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval31.TabIndex = 30;
            // 
            // labelVarInterval32
            // 
            this.labelVarInterval32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval32.AutoSize = true;
            this.labelVarInterval32.Location = new System.Drawing.Point(237, 305);
            this.labelVarInterval32.Name = "labelVarInterval32";
            this.labelVarInterval32.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval32.TabIndex = 81;
            this.labelVarInterval32.Text = "label42";
            this.labelVarInterval32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval33
            // 
            this.labelVarInterval33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval33.AutoSize = true;
            this.labelVarInterval33.Location = new System.Drawing.Point(471, 305);
            this.labelVarInterval33.Name = "labelVarInterval33";
            this.labelVarInterval33.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval33.TabIndex = 82;
            this.labelVarInterval33.Text = "label43";
            this.labelVarInterval33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval34
            // 
            this.labelVarInterval34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval34.AutoSize = true;
            this.labelVarInterval34.Location = new System.Drawing.Point(705, 305);
            this.labelVarInterval34.Name = "labelVarInterval34";
            this.labelVarInterval34.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval34.TabIndex = 83;
            this.labelVarInterval34.Text = "label44";
            this.labelVarInterval34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval35
            // 
            this.labelVarInterval35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval35.AutoSize = true;
            this.labelVarInterval35.Location = new System.Drawing.Point(939, 305);
            this.labelVarInterval35.Name = "labelVarInterval35";
            this.labelVarInterval35.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval35.TabIndex = 84;
            this.labelVarInterval35.Text = "label45";
            this.labelVarInterval35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval36
            // 
            this.labelVarInterval36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval36.AutoSize = true;
            this.labelVarInterval36.Location = new System.Drawing.Point(3, 353);
            this.labelVarInterval36.Name = "labelVarInterval36";
            this.labelVarInterval36.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval36.TabIndex = 85;
            this.labelVarInterval36.Text = "label46";
            this.labelVarInterval36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval37
            // 
            this.labelVarInterval37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval37.AutoSize = true;
            this.labelVarInterval37.Location = new System.Drawing.Point(237, 353);
            this.labelVarInterval37.Name = "labelVarInterval37";
            this.labelVarInterval37.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval37.TabIndex = 86;
            this.labelVarInterval37.Text = "label47";
            this.labelVarInterval37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval38
            // 
            this.labelVarInterval38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval38.AutoSize = true;
            this.labelVarInterval38.Location = new System.Drawing.Point(471, 353);
            this.labelVarInterval38.Name = "labelVarInterval38";
            this.labelVarInterval38.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval38.TabIndex = 87;
            this.labelVarInterval38.Text = "label48";
            this.labelVarInterval38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval39
            // 
            this.labelVarInterval39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval39.AutoSize = true;
            this.labelVarInterval39.Location = new System.Drawing.Point(705, 353);
            this.labelVarInterval39.Name = "labelVarInterval39";
            this.labelVarInterval39.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval39.TabIndex = 88;
            this.labelVarInterval39.Text = "label49";
            this.labelVarInterval39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval40
            // 
            this.labelVarInterval40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval40.AutoSize = true;
            this.labelVarInterval40.Location = new System.Drawing.Point(939, 353);
            this.labelVarInterval40.Name = "labelVarInterval40";
            this.labelVarInterval40.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval40.TabIndex = 89;
            this.labelVarInterval40.Text = "label50";
            this.labelVarInterval40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval41
            // 
            this.labelVarInterval41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval41.AutoSize = true;
            this.labelVarInterval41.Location = new System.Drawing.Point(3, 401);
            this.labelVarInterval41.Name = "labelVarInterval41";
            this.labelVarInterval41.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval41.TabIndex = 90;
            this.labelVarInterval41.Text = "label51";
            this.labelVarInterval41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval42
            // 
            this.labelVarInterval42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval42.AutoSize = true;
            this.labelVarInterval42.Location = new System.Drawing.Point(237, 401);
            this.labelVarInterval42.Name = "labelVarInterval42";
            this.labelVarInterval42.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval42.TabIndex = 91;
            this.labelVarInterval42.Text = "label52";
            this.labelVarInterval42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval43
            // 
            this.labelVarInterval43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval43.AutoSize = true;
            this.labelVarInterval43.Location = new System.Drawing.Point(471, 401);
            this.labelVarInterval43.Name = "labelVarInterval43";
            this.labelVarInterval43.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval43.TabIndex = 92;
            this.labelVarInterval43.Text = "label53";
            this.labelVarInterval43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval44
            // 
            this.labelVarInterval44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval44.AutoSize = true;
            this.labelVarInterval44.Location = new System.Drawing.Point(705, 401);
            this.labelVarInterval44.Name = "labelVarInterval44";
            this.labelVarInterval44.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval44.TabIndex = 93;
            this.labelVarInterval44.Text = "label54";
            this.labelVarInterval44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval45
            // 
            this.labelVarInterval45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval45.AutoSize = true;
            this.labelVarInterval45.Location = new System.Drawing.Point(939, 401);
            this.labelVarInterval45.Name = "labelVarInterval45";
            this.labelVarInterval45.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval45.TabIndex = 94;
            this.labelVarInterval45.Text = "label55";
            this.labelVarInterval45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval47
            // 
            this.labelVarInterval47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval47.AutoSize = true;
            this.labelVarInterval47.Location = new System.Drawing.Point(237, 454);
            this.labelVarInterval47.Name = "labelVarInterval47";
            this.labelVarInterval47.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval47.TabIndex = 96;
            this.labelVarInterval47.Text = "label57";
            this.labelVarInterval47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval48
            // 
            this.labelVarInterval48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval48.AutoSize = true;
            this.labelVarInterval48.Location = new System.Drawing.Point(471, 454);
            this.labelVarInterval48.Name = "labelVarInterval48";
            this.labelVarInterval48.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval48.TabIndex = 9;
            this.labelVarInterval48.Text = "label58";
            this.labelVarInterval48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval49
            // 
            this.labelVarInterval49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval49.AutoSize = true;
            this.labelVarInterval49.Location = new System.Drawing.Point(705, 454);
            this.labelVarInterval49.Name = "labelVarInterval49";
            this.labelVarInterval49.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval49.TabIndex = 98;
            this.labelVarInterval49.Text = "label59";
            this.labelVarInterval49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelVarInterval50
            // 
            this.labelVarInterval50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVarInterval50.AutoSize = true;
            this.labelVarInterval50.Location = new System.Drawing.Point(939, 454);
            this.labelVarInterval50.Name = "labelVarInterval50";
            this.labelVarInterval50.Size = new System.Drawing.Size(111, 13);
            this.labelVarInterval50.TabIndex = 99;
            this.labelVarInterval50.Text = "label60";
            this.labelVarInterval50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxVarInterval32
            // 
            this.textBoxVarInterval32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval32.Location = new System.Drawing.Point(354, 302);
            this.textBoxVarInterval32.Name = "textBoxVarInterval32";
            this.textBoxVarInterval32.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval32.TabIndex = 31;
            // 
            // textBoxVarInterval33
            // 
            this.textBoxVarInterval33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval33.Location = new System.Drawing.Point(588, 302);
            this.textBoxVarInterval33.Name = "textBoxVarInterval33";
            this.textBoxVarInterval33.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval33.TabIndex = 32;
            // 
            // textBoxVarInterval34
            // 
            this.textBoxVarInterval34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval34.Location = new System.Drawing.Point(822, 302);
            this.textBoxVarInterval34.Name = "textBoxVarInterval34";
            this.textBoxVarInterval34.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval34.TabIndex = 33;
            // 
            // textBoxVarInterval35
            // 
            this.textBoxVarInterval35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval35.Location = new System.Drawing.Point(1056, 302);
            this.textBoxVarInterval35.Name = "textBoxVarInterval35";
            this.textBoxVarInterval35.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval35.TabIndex = 34;
            // 
            // textBoxVarInterval36
            // 
            this.textBoxVarInterval36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval36.Location = new System.Drawing.Point(120, 350);
            this.textBoxVarInterval36.Name = "textBoxVarInterval36";
            this.textBoxVarInterval36.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval36.TabIndex = 35;
            // 
            // textBoxVarInterval37
            // 
            this.textBoxVarInterval37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval37.Location = new System.Drawing.Point(354, 350);
            this.textBoxVarInterval37.Name = "textBoxVarInterval37";
            this.textBoxVarInterval37.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval37.TabIndex = 36;
            // 
            // textBoxVarInterval38
            // 
            this.textBoxVarInterval38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval38.Location = new System.Drawing.Point(588, 350);
            this.textBoxVarInterval38.Name = "textBoxVarInterval38";
            this.textBoxVarInterval38.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval38.TabIndex = 37;
            // 
            // textBoxVarInterval39
            // 
            this.textBoxVarInterval39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval39.Location = new System.Drawing.Point(822, 350);
            this.textBoxVarInterval39.Name = "textBoxVarInterval39";
            this.textBoxVarInterval39.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval39.TabIndex = 38;
            // 
            // textBoxVarInterval40
            // 
            this.textBoxVarInterval40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval40.Location = new System.Drawing.Point(1056, 350);
            this.textBoxVarInterval40.Name = "textBoxVarInterval40";
            this.textBoxVarInterval40.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval40.TabIndex = 39;
            // 
            // textBoxVarInterval41
            // 
            this.textBoxVarInterval41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval41.Location = new System.Drawing.Point(120, 398);
            this.textBoxVarInterval41.Name = "textBoxVarInterval41";
            this.textBoxVarInterval41.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval41.TabIndex = 40;
            // 
            // textBoxVarInterval42
            // 
            this.textBoxVarInterval42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval42.Location = new System.Drawing.Point(354, 398);
            this.textBoxVarInterval42.Name = "textBoxVarInterval42";
            this.textBoxVarInterval42.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval42.TabIndex = 41;
            // 
            // textBoxVarInterval43
            // 
            this.textBoxVarInterval43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval43.Location = new System.Drawing.Point(588, 398);
            this.textBoxVarInterval43.Name = "textBoxVarInterval43";
            this.textBoxVarInterval43.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval43.TabIndex = 42;
            // 
            // textBoxVarInterval44
            // 
            this.textBoxVarInterval44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval44.Location = new System.Drawing.Point(822, 398);
            this.textBoxVarInterval44.Name = "textBoxVarInterval44";
            this.textBoxVarInterval44.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval44.TabIndex = 43;
            // 
            // textBoxVarInterval45
            // 
            this.textBoxVarInterval45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval45.Location = new System.Drawing.Point(1056, 398);
            this.textBoxVarInterval45.Name = "textBoxVarInterval45";
            this.textBoxVarInterval45.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval45.TabIndex = 44;
            // 
            // textBoxVarInterval46
            // 
            this.textBoxVarInterval46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval46.Location = new System.Drawing.Point(120, 450);
            this.textBoxVarInterval46.Name = "textBoxVarInterval46";
            this.textBoxVarInterval46.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval46.TabIndex = 45;
            // 
            // textBoxVarInterval47
            // 
            this.textBoxVarInterval47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval47.Location = new System.Drawing.Point(354, 450);
            this.textBoxVarInterval47.Name = "textBoxVarInterval47";
            this.textBoxVarInterval47.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval47.TabIndex = 46;
            // 
            // textBoxVarInterval48
            // 
            this.textBoxVarInterval48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval48.Location = new System.Drawing.Point(588, 450);
            this.textBoxVarInterval48.Name = "textBoxVarInterval48";
            this.textBoxVarInterval48.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval48.TabIndex = 47;
            // 
            // textBoxVarInterval49
            // 
            this.textBoxVarInterval49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval49.Location = new System.Drawing.Point(822, 450);
            this.textBoxVarInterval49.Name = "textBoxVarInterval49";
            this.textBoxVarInterval49.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval49.TabIndex = 48;
            // 
            // textBoxVarInterval50
            // 
            this.textBoxVarInterval50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval50.Location = new System.Drawing.Point(1056, 450);
            this.textBoxVarInterval50.Name = "textBoxVarInterval50";
            this.textBoxVarInterval50.Size = new System.Drawing.Size(113, 20);
            this.textBoxVarInterval50.TabIndex = 49;
            // 
            // textBoxVarInterval24
            // 
            this.textBoxVarInterval24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxVarInterval24.Location = new System.Drawing.Point(822, 206);
            this.textBoxVarInterval24.Name = "textBoxVarInterval24";
            this.textBoxVarInterval24.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarInterval24.TabIndex = 23;
            // 
            // tabPageMaodule
            // 
            this.tabPageMaodule.Controls.Add(this.tableLayoutPanel12);
            this.tabPageMaodule.Location = new System.Drawing.Point(4, 22);
            this.tabPageMaodule.Name = "tabPageMaodule";
            this.tabPageMaodule.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMaodule.Size = new System.Drawing.Size(1359, 507);
            this.tabPageMaodule.TabIndex = 2;
            this.tabPageMaodule.Text = "Magnitudes";
            this.tabPageMaodule.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 268F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1353, 501);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.Controls.Add(this.comboBoxConf3, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.comboBoxConf2, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.checkBoxConf1, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.checkBoxConf2, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.checkBoxConf3, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.comboBoxConf1, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(262, 100);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // comboBoxConf3
            // 
            this.comboBoxConf3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxConf3.FormattingEnabled = true;
            this.comboBoxConf3.Location = new System.Drawing.Point(3, 72);
            this.comboBoxConf3.Name = "comboBoxConf3";
            this.comboBoxConf3.Size = new System.Drawing.Size(216, 21);
            this.comboBoxConf3.TabIndex = 5;
            // 
            // comboBoxConf2
            // 
            this.comboBoxConf2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxConf2.FormattingEnabled = true;
            this.comboBoxConf2.Location = new System.Drawing.Point(3, 39);
            this.comboBoxConf2.Name = "comboBoxConf2";
            this.comboBoxConf2.Size = new System.Drawing.Size(216, 21);
            this.comboBoxConf2.TabIndex = 4;
            // 
            // checkBoxConf1
            // 
            this.checkBoxConf1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxConf1.AutoSize = true;
            this.checkBoxConf1.Location = new System.Drawing.Point(234, 9);
            this.checkBoxConf1.Name = "checkBoxConf1";
            this.checkBoxConf1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxConf1.TabIndex = 0;
            this.checkBoxConf1.UseVisualStyleBackColor = true;
            // 
            // checkBoxConf2
            // 
            this.checkBoxConf2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxConf2.AutoSize = true;
            this.checkBoxConf2.Location = new System.Drawing.Point(234, 42);
            this.checkBoxConf2.Name = "checkBoxConf2";
            this.checkBoxConf2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxConf2.TabIndex = 1;
            this.checkBoxConf2.UseVisualStyleBackColor = true;
            // 
            // checkBoxConf3
            // 
            this.checkBoxConf3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxConf3.AutoSize = true;
            this.checkBoxConf3.Location = new System.Drawing.Point(234, 76);
            this.checkBoxConf3.Name = "checkBoxConf3";
            this.checkBoxConf3.Size = new System.Drawing.Size(15, 14);
            this.checkBoxConf3.TabIndex = 2;
            this.checkBoxConf3.UseVisualStyleBackColor = true;
            // 
            // comboBoxConf1
            // 
            this.comboBoxConf1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxConf1.FormattingEnabled = true;
            this.comboBoxConf1.Location = new System.Drawing.Point(3, 6);
            this.comboBoxConf1.Name = "comboBoxConf1";
            this.comboBoxConf1.Size = new System.Drawing.Size(216, 21);
            this.comboBoxConf1.TabIndex = 3;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 5;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.Controls.Add(this.listViewConf3, 4, 0);
            this.tableLayoutPanel14.Controls.Add(this.listViewConf2, 3, 0);
            this.tableLayoutPanel14.Controls.Add(this.listViewConf1, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.listViewOriginalNumbers, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.listView2, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(271, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1079, 495);
            this.tableLayoutPanel14.TabIndex = 1;
            // 
            // listViewConf3
            // 
            this.listViewConf3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewConf3.Location = new System.Drawing.Point(832, 3);
            this.listViewConf3.Name = "listViewConf3";
            this.listViewConf3.Size = new System.Drawing.Size(244, 489);
            this.listViewConf3.TabIndex = 3;
            this.listViewConf3.UseCompatibleStateImageBehavior = false;
            // 
            // listViewConf2
            // 
            this.listViewConf2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewConf2.Location = new System.Drawing.Point(585, 3);
            this.listViewConf2.Name = "listViewConf2";
            this.listViewConf2.Size = new System.Drawing.Size(241, 489);
            this.listViewConf2.TabIndex = 2;
            this.listViewConf2.UseCompatibleStateImageBehavior = false;
            // 
            // listViewConf1
            // 
            this.listViewConf1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewConf1.Location = new System.Drawing.Point(338, 3);
            this.listViewConf1.Name = "listViewConf1";
            this.listViewConf1.Size = new System.Drawing.Size(241, 489);
            this.listViewConf1.TabIndex = 1;
            this.listViewConf1.UseCompatibleStateImageBehavior = false;
            // 
            // listViewOriginalNumbers
            // 
            this.listViewOriginalNumbers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewOriginalNumbers.Location = new System.Drawing.Point(91, 3);
            this.listViewOriginalNumbers.Name = "listViewOriginalNumbers";
            this.listViewOriginalNumbers.Size = new System.Drawing.Size(241, 489);
            this.listViewOriginalNumbers.TabIndex = 0;
            this.listViewOriginalNumbers.UseCompatibleStateImageBehavior = false;
            // 
            // listView2
            // 
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Location = new System.Drawing.Point(3, 3);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(82, 489);
            this.listView2.TabIndex = 4;
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // tabPageGraphics
            // 
            this.tabPageGraphics.Controls.Add(this.tableLayoutPanel4);
            this.tabPageGraphics.Location = new System.Drawing.Point(4, 22);
            this.tabPageGraphics.Name = "tabPageGraphics";
            this.tabPageGraphics.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGraphics.Size = new System.Drawing.Size(1359, 507);
            this.tabPageGraphics.TabIndex = 3;
            this.tabPageGraphics.Text = "Graficos";
            this.tabPageGraphics.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1353, 501);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 243F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel17, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1347, 495);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel16, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(237, 489);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel15.Controls.Add(this.comboBoxGraphicConf3, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.comboBoxGraphicConf2, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.checkBoxGraphicConf1, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.checkBoxGraphicConf2, 1, 1);
            this.tableLayoutPanel15.Controls.Add(this.checkBoxGraphicConf3, 1, 2);
            this.tableLayoutPanel15.Controls.Add(this.comboBoxGraphicConf1, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 3;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(231, 114);
            this.tableLayoutPanel15.TabIndex = 1;
            // 
            // comboBoxGraphicConf3
            // 
            this.comboBoxGraphicConf3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxGraphicConf3.FormattingEnabled = true;
            this.comboBoxGraphicConf3.Location = new System.Drawing.Point(3, 84);
            this.comboBoxGraphicConf3.Name = "comboBoxGraphicConf3";
            this.comboBoxGraphicConf3.Size = new System.Drawing.Size(185, 21);
            this.comboBoxGraphicConf3.TabIndex = 5;
            // 
            // comboBoxGraphicConf2
            // 
            this.comboBoxGraphicConf2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxGraphicConf2.FormattingEnabled = true;
            this.comboBoxGraphicConf2.Location = new System.Drawing.Point(3, 46);
            this.comboBoxGraphicConf2.Name = "comboBoxGraphicConf2";
            this.comboBoxGraphicConf2.Size = new System.Drawing.Size(185, 21);
            this.comboBoxGraphicConf2.TabIndex = 4;
            // 
            // checkBoxGraphicConf1
            // 
            this.checkBoxGraphicConf1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxGraphicConf1.AutoSize = true;
            this.checkBoxGraphicConf1.Location = new System.Drawing.Point(203, 12);
            this.checkBoxGraphicConf1.Name = "checkBoxGraphicConf1";
            this.checkBoxGraphicConf1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGraphicConf1.TabIndex = 0;
            this.checkBoxGraphicConf1.UseVisualStyleBackColor = true;
            // 
            // checkBoxGraphicConf2
            // 
            this.checkBoxGraphicConf2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxGraphicConf2.AutoSize = true;
            this.checkBoxGraphicConf2.Location = new System.Drawing.Point(203, 50);
            this.checkBoxGraphicConf2.Name = "checkBoxGraphicConf2";
            this.checkBoxGraphicConf2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGraphicConf2.TabIndex = 1;
            this.checkBoxGraphicConf2.UseVisualStyleBackColor = true;
            // 
            // checkBoxGraphicConf3
            // 
            this.checkBoxGraphicConf3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxGraphicConf3.AutoSize = true;
            this.checkBoxGraphicConf3.Location = new System.Drawing.Point(203, 88);
            this.checkBoxGraphicConf3.Name = "checkBoxGraphicConf3";
            this.checkBoxGraphicConf3.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGraphicConf3.TabIndex = 2;
            this.checkBoxGraphicConf3.UseVisualStyleBackColor = true;
            // 
            // comboBoxGraphicConf1
            // 
            this.comboBoxGraphicConf1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxGraphicConf1.FormattingEnabled = true;
            this.comboBoxGraphicConf1.Location = new System.Drawing.Point(3, 8);
            this.comboBoxGraphicConf1.Name = "comboBoxGraphicConf1";
            this.comboBoxGraphicConf1.Size = new System.Drawing.Size(185, 21);
            this.comboBoxGraphicConf1.TabIndex = 3;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.radioButtonVisibility, 0, 10);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonReducPression, 0, 7);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonAirHumidity, 0, 6);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonThermalSensation, 0, 5);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonWindSpeed, 0, 4);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonClouds, 0, 3);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonRainChance, 0, 2);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonUVRadiation, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonTemperature, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonWind, 0, 8);
            this.tableLayoutPanel16.Controls.Add(this.radioButtonAccRain, 0, 9);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 123);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 11;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(231, 363);
            this.tableLayoutPanel16.TabIndex = 2;
            // 
            // radioButtonVisibility
            // 
            this.radioButtonVisibility.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonVisibility.AutoSize = true;
            this.radioButtonVisibility.Location = new System.Drawing.Point(3, 333);
            this.radioButtonVisibility.Name = "radioButtonVisibility";
            this.radioButtonVisibility.Size = new System.Drawing.Size(77, 17);
            this.radioButtonVisibility.TabIndex = 10;
            this.radioButtonVisibility.TabStop = true;
            this.radioButtonVisibility.Text = "Visibilidade";
            this.radioButtonVisibility.UseVisualStyleBackColor = true;
            // 
            // radioButtonReducPression
            // 
            this.radioButtonReducPression.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonReducPression.AutoSize = true;
            this.radioButtonReducPression.Location = new System.Drawing.Point(3, 231);
            this.radioButtonReducPression.Name = "radioButtonReducPression";
            this.radioButtonReducPression.Size = new System.Drawing.Size(111, 17);
            this.radioButtonReducPression.TabIndex = 7;
            this.radioButtonReducPression.TabStop = true;
            this.radioButtonReducPression.Text = "Pressao Reduzida";
            this.radioButtonReducPression.UseVisualStyleBackColor = true;
            // 
            // radioButtonAirHumidity
            // 
            this.radioButtonAirHumidity.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonAirHumidity.AutoSize = true;
            this.radioButtonAirHumidity.Location = new System.Drawing.Point(3, 199);
            this.radioButtonAirHumidity.Name = "radioButtonAirHumidity";
            this.radioButtonAirHumidity.Size = new System.Drawing.Size(101, 17);
            this.radioButtonAirHumidity.TabIndex = 6;
            this.radioButtonAirHumidity.TabStop = true;
            this.radioButtonAirHumidity.Text = "Humidade do Ar";
            this.radioButtonAirHumidity.UseVisualStyleBackColor = true;
            // 
            // radioButtonThermalSensation
            // 
            this.radioButtonThermalSensation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonThermalSensation.AutoSize = true;
            this.radioButtonThermalSensation.Location = new System.Drawing.Point(3, 167);
            this.radioButtonThermalSensation.Name = "radioButtonThermalSensation";
            this.radioButtonThermalSensation.Size = new System.Drawing.Size(114, 17);
            this.radioButtonThermalSensation.TabIndex = 5;
            this.radioButtonThermalSensation.TabStop = true;
            this.radioButtonThermalSensation.Text = "Sensacao Termica";
            this.radioButtonThermalSensation.UseVisualStyleBackColor = true;
            // 
            // radioButtonWindSpeed
            // 
            this.radioButtonWindSpeed.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonWindSpeed.AutoSize = true;
            this.radioButtonWindSpeed.Location = new System.Drawing.Point(3, 135);
            this.radioButtonWindSpeed.Name = "radioButtonWindSpeed";
            this.radioButtonWindSpeed.Size = new System.Drawing.Size(124, 17);
            this.radioButtonWindSpeed.TabIndex = 4;
            this.radioButtonWindSpeed.TabStop = true;
            this.radioButtonWindSpeed.Text = "Velocidade do Vento";
            this.radioButtonWindSpeed.UseVisualStyleBackColor = true;
            // 
            // radioButtonClouds
            // 
            this.radioButtonClouds.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonClouds.AutoSize = true;
            this.radioButtonClouds.Location = new System.Drawing.Point(3, 103);
            this.radioButtonClouds.Name = "radioButtonClouds";
            this.radioButtonClouds.Size = new System.Drawing.Size(62, 17);
            this.radioButtonClouds.TabIndex = 3;
            this.radioButtonClouds.TabStop = true;
            this.radioButtonClouds.Text = "Nuvens";
            this.radioButtonClouds.UseVisualStyleBackColor = true;
            // 
            // radioButtonRainChance
            // 
            this.radioButtonRainChance.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonRainChance.AutoSize = true;
            this.radioButtonRainChance.Location = new System.Drawing.Point(3, 71);
            this.radioButtonRainChance.Name = "radioButtonRainChance";
            this.radioButtonRainChance.Size = new System.Drawing.Size(124, 17);
            this.radioButtonRainChance.TabIndex = 2;
            this.radioButtonRainChance.TabStop = true;
            this.radioButtonRainChance.Text = "Chance Precipitacao";
            this.radioButtonRainChance.UseVisualStyleBackColor = true;
            // 
            // radioButtonUVRadiation
            // 
            this.radioButtonUVRadiation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonUVRadiation.AutoSize = true;
            this.radioButtonUVRadiation.Location = new System.Drawing.Point(3, 39);
            this.radioButtonUVRadiation.Name = "radioButtonUVRadiation";
            this.radioButtonUVRadiation.Size = new System.Drawing.Size(72, 17);
            this.radioButtonUVRadiation.TabIndex = 1;
            this.radioButtonUVRadiation.TabStop = true;
            this.radioButtonUVRadiation.Text = "Indice UV";
            this.radioButtonUVRadiation.UseVisualStyleBackColor = true;
            // 
            // radioButtonTemperature
            // 
            this.radioButtonTemperature.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonTemperature.AutoSize = true;
            this.radioButtonTemperature.Location = new System.Drawing.Point(3, 7);
            this.radioButtonTemperature.Name = "radioButtonTemperature";
            this.radioButtonTemperature.Size = new System.Drawing.Size(85, 17);
            this.radioButtonTemperature.TabIndex = 0;
            this.radioButtonTemperature.TabStop = true;
            this.radioButtonTemperature.Text = "Temperatura";
            this.radioButtonTemperature.UseVisualStyleBackColor = true;
            // 
            // radioButtonWind
            // 
            this.radioButtonWind.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonWind.AutoSize = true;
            this.radioButtonWind.Location = new System.Drawing.Point(3, 263);
            this.radioButtonWind.Name = "radioButtonWind";
            this.radioButtonWind.Size = new System.Drawing.Size(59, 17);
            this.radioButtonWind.TabIndex = 8;
            this.radioButtonWind.TabStop = true;
            this.radioButtonWind.Text = "Rajada";
            this.radioButtonWind.UseVisualStyleBackColor = true;
            // 
            // radioButtonAccRain
            // 
            this.radioButtonAccRain.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonAccRain.AutoSize = true;
            this.radioButtonAccRain.Location = new System.Drawing.Point(3, 295);
            this.radioButtonAccRain.Name = "radioButtonAccRain";
            this.radioButtonAccRain.Size = new System.Drawing.Size(140, 17);
            this.radioButtonAccRain.TabIndex = 9;
            this.radioButtonAccRain.TabStop = true;
            this.radioButtonAccRain.Text = "Precipitacao Acumulada";
            this.radioButtonAccRain.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel18, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel19, 0, 2);
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel20, 0, 4);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(246, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 6;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1098, 489);
            this.tableLayoutPanel17.TabIndex = 1;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.checkBoxEnableGraphic1, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1092, 29);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // checkBoxEnableGraphic1
            // 
            this.checkBoxEnableGraphic1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxEnableGraphic1.AutoSize = true;
            this.checkBoxEnableGraphic1.Location = new System.Drawing.Point(10, 7);
            this.checkBoxEnableGraphic1.Name = "checkBoxEnableGraphic1";
            this.checkBoxEnableGraphic1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnableGraphic1.TabIndex = 0;
            this.checkBoxEnableGraphic1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(507, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 24);
            this.label6.TabIndex = 1;
            this.label6.Text = "GRAFICO1";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.checkBoxEnableGraphic2, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 166);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1092, 29);
            this.tableLayoutPanel19.TabIndex = 1;
            // 
            // checkBoxEnableGraphic2
            // 
            this.checkBoxEnableGraphic2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxEnableGraphic2.AutoSize = true;
            this.checkBoxEnableGraphic2.Location = new System.Drawing.Point(10, 7);
            this.checkBoxEnableGraphic2.Name = "checkBoxEnableGraphic2";
            this.checkBoxEnableGraphic2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnableGraphic2.TabIndex = 0;
            this.checkBoxEnableGraphic2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(507, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 24);
            this.label7.TabIndex = 1;
            this.label7.Text = "GRAFICO2";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Controls.Add(this.checkBoxEnableGraphic3, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 329);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1092, 29);
            this.tableLayoutPanel20.TabIndex = 2;
            // 
            // checkBoxEnableGraphic3
            // 
            this.checkBoxEnableGraphic3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxEnableGraphic3.AutoSize = true;
            this.checkBoxEnableGraphic3.Location = new System.Drawing.Point(10, 7);
            this.checkBoxEnableGraphic3.Name = "checkBoxEnableGraphic3";
            this.checkBoxEnableGraphic3.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEnableGraphic3.TabIndex = 0;
            this.checkBoxEnableGraphic3.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(507, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "GRAFICO3";
            // 
            // dataGridViewDataRead
            // 
            this.dataGridViewDataRead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDataRead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDataRead.Location = new System.Drawing.Point(3, 98);
            this.dataGridViewDataRead.Name = "dataGridViewDataRead";
            this.dataGridViewDataRead.Size = new System.Drawing.Size(1341, 394);
            this.dataGridViewDataRead.TabIndex = 1;
            // 
            // SolarEnergy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1379, 610);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(1147, 649);
            this.Name = "SolarEnergy";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tabControlSolarEnergy.ResumeLayout(false);
            this.tabPageData.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tabPageCalc.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tabPageMaodule.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tabPageGraphics.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataRead)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonSetData;
        private System.Windows.Forms.TabControl tabControlSolarEnergy;
        private System.Windows.Forms.TabPage tabPageData;
        private System.Windows.Forms.TabPage tabPageCalc;
        private System.Windows.Forms.TabPage tabPageMaodule;
        private System.Windows.Forms.TabPage tabPageGraphics;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.CheckBox checkBoxPrevisionSolarRule;
        private System.Windows.Forms.CheckBox checkBoxPrevisionSolar;
        private System.Windows.Forms.CheckBox checkBoxPrevisionWind;
        private System.Windows.Forms.CheckBox checkBoxPrevisionReducPression;
        private System.Windows.Forms.CheckBox checkBoxPrevisionAirHumidity;
        private System.Windows.Forms.CheckBox checkBoxPrevisionThermalSensation;
        private System.Windows.Forms.CheckBox checkBoxPrevisionTemp;
        private System.Windows.Forms.CheckBox checkBoxDataPot;
        private System.Windows.Forms.CheckBox checkBoxPrevisionRainChance;
        private System.Windows.Forms.CheckBox checkBoxPrevisionClouds;
        private System.Windows.Forms.CheckBox checkBoxPrevisionWindSpeed;
        private System.Windows.Forms.CheckBox checkBoxPrevisionUVRadiation;
        private System.Windows.Forms.CheckBox checkBoxDataRadiation;
        private System.Windows.Forms.CheckBox checkBoxPrevisionVisibility;
        private System.Windows.Forms.CheckBox checkBoxPrevisionAccRain;
        private System.Windows.Forms.Button buttonGetData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.ComboBox comboBoxConf3;
        private System.Windows.Forms.ComboBox comboBoxConf2;
        private System.Windows.Forms.CheckBox checkBoxConf1;
        private System.Windows.Forms.CheckBox checkBoxConf2;
        private System.Windows.Forms.CheckBox checkBoxConf3;
        private System.Windows.Forms.ComboBox comboBoxConf1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.ListView listViewConf3;
        private System.Windows.Forms.ListView listViewConf2;
        private System.Windows.Forms.ListView listViewConf1;
        private System.Windows.Forms.ListView listViewOriginalNumbers;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.ComboBox comboBoxGraphicConf3;
        private System.Windows.Forms.ComboBox comboBoxGraphicConf2;
        private System.Windows.Forms.CheckBox checkBoxGraphicConf1;
        private System.Windows.Forms.CheckBox checkBoxGraphicConf2;
        private System.Windows.Forms.CheckBox checkBoxGraphicConf3;
        private System.Windows.Forms.ComboBox comboBoxGraphicConf1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.RadioButton radioButtonVisibility;
        private System.Windows.Forms.RadioButton radioButtonReducPression;
        private System.Windows.Forms.RadioButton radioButtonAirHumidity;
        private System.Windows.Forms.RadioButton radioButtonThermalSensation;
        private System.Windows.Forms.RadioButton radioButtonWindSpeed;
        private System.Windows.Forms.RadioButton radioButtonClouds;
        private System.Windows.Forms.RadioButton radioButtonRainChance;
        private System.Windows.Forms.RadioButton radioButtonUVRadiation;
        private System.Windows.Forms.RadioButton radioButtonTemperature;
        private System.Windows.Forms.RadioButton radioButtonWind;
        private System.Windows.Forms.RadioButton radioButtonAccRain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.CheckBox checkBoxEnableGraphic1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.CheckBox checkBoxEnableGraphic2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.CheckBox checkBoxEnableGraphic3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonGetVariableInterval;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Label labelVarInterval26;
        private System.Windows.Forms.TextBox textBoxVarInterval26;
        private System.Windows.Forms.Label labelVarInterval25;
        private System.Windows.Forms.TextBox textBoxVarInterval25;
        private System.Windows.Forms.Label labelVarInterval24;
        private System.Windows.Forms.TextBox textBoxVarInterval24;
        private System.Windows.Forms.Label labelVarInterval23;
        private System.Windows.Forms.TextBox textBoxVarInterval23;
        private System.Windows.Forms.Label labelVarInterval22;
        private System.Windows.Forms.TextBox textBoxVarInterval22;
        private System.Windows.Forms.Label labelVarInterval21;
        private System.Windows.Forms.TextBox textBoxVarInterval21;
        private System.Windows.Forms.Label labelVarInterval20;
        private System.Windows.Forms.TextBox textBoxVarInterval20;
        private System.Windows.Forms.Label labelVarInterval19;
        private System.Windows.Forms.TextBox textBoxVarInterval19;
        private System.Windows.Forms.Label labelVarInterval18;
        private System.Windows.Forms.TextBox textBoxVarInterval18;
        private System.Windows.Forms.Label labelVarInterval17;
        private System.Windows.Forms.TextBox textBoxVarInterval17;
        private System.Windows.Forms.Label labelVarInterval16;
        private System.Windows.Forms.TextBox textBoxVarInterval16;
        private System.Windows.Forms.Label labelVarInterval15;
        private System.Windows.Forms.TextBox textBoxVarInterval15;
        private System.Windows.Forms.Label labelVarInterval14;
        private System.Windows.Forms.TextBox textBoxVarInterval14;
        private System.Windows.Forms.Label labelVarInterval13;
        private System.Windows.Forms.TextBox textBoxVarInterval13;
        private System.Windows.Forms.Label labelVarInterval12;
        private System.Windows.Forms.TextBox textBoxVarInterval12;
        private System.Windows.Forms.Label llabelVarInterval11;
        private System.Windows.Forms.TextBox textBoxVarInterval11;
        private System.Windows.Forms.Label labelVarInterval10;
        private System.Windows.Forms.TextBox textBoxVarInterval10;
        private System.Windows.Forms.Label labelVarInterval9;
        private System.Windows.Forms.TextBox textBoxVarInterval9;
        private System.Windows.Forms.Label labelVarInterval8;
        private System.Windows.Forms.TextBox extBoxVarInterval58;
        private System.Windows.Forms.Label labelVarInterval7;
        private System.Windows.Forms.TextBox textBoxVarInterval7;
        private System.Windows.Forms.Label labelVarInterval6;
        private System.Windows.Forms.TextBox textBoxVarInterval6;
        private System.Windows.Forms.Label labelVarInterval5;
        private System.Windows.Forms.TextBox textBoxVarInterval5;
        private System.Windows.Forms.Label labelVarInterval4;
        private System.Windows.Forms.TextBox textBoxVarInterval4;
        private System.Windows.Forms.Label labelVarInterval3;
        private System.Windows.Forms.TextBox textBoxVarInterval3;
        private System.Windows.Forms.Label labelVarInterval2;
        private System.Windows.Forms.TextBox textBoxVarInterval2;
        private System.Windows.Forms.Label labelVarInterval1;
        private System.Windows.Forms.TextBox textBoxVarInterval1;
        private System.Windows.Forms.Label labelVarInterval27;
        private System.Windows.Forms.TextBox textBoxVarInterval27;
        private System.Windows.Forms.Label labelVarInterval28;
        private System.Windows.Forms.TextBox textBoxVarInterval28;
        private System.Windows.Forms.Label labelVarInterval29;
        private System.Windows.Forms.TextBox textBoxVarInterval29;
        private System.Windows.Forms.Label labelVarInterval30;
        private System.Windows.Forms.TextBox textBoxVarInterval30;
        private System.Windows.Forms.Label labelVarInterval31;
        private System.Windows.Forms.TextBox textBoxVarInterval31;
        private System.Windows.Forms.Label labelVarInterval46;
        private System.Windows.Forms.Label labelVarInterval32;
        private System.Windows.Forms.Label labelVarInterval33;
        private System.Windows.Forms.Label labelVarInterval34;
        private System.Windows.Forms.Label labelVarInterval35;
        private System.Windows.Forms.Label labelVarInterval36;
        private System.Windows.Forms.Label labelVarInterval37;
        private System.Windows.Forms.Label labelVarInterval38;
        private System.Windows.Forms.Label labelVarInterval39;
        private System.Windows.Forms.Label labelVarInterval40;
        private System.Windows.Forms.Label labelVarInterval41;
        private System.Windows.Forms.Label labelVarInterval42;
        private System.Windows.Forms.Label labelVarInterval43;
        private System.Windows.Forms.Label labelVarInterval44;
        private System.Windows.Forms.Label labelVarInterval45;
        private System.Windows.Forms.Label labelVarInterval47;
        private System.Windows.Forms.Label labelVarInterval48;
        private System.Windows.Forms.Label labelVarInterval49;
        private System.Windows.Forms.Label labelVarInterval50;
        private System.Windows.Forms.TextBox textBoxVarInterval32;
        private System.Windows.Forms.TextBox textBoxVarInterval33;
        private System.Windows.Forms.TextBox textBoxVarInterval34;
        private System.Windows.Forms.TextBox textBoxVarInterval35;
        private System.Windows.Forms.TextBox textBoxVarInterval36;
        private System.Windows.Forms.TextBox textBoxVarInterval37;
        private System.Windows.Forms.TextBox textBoxVarInterval38;
        private System.Windows.Forms.TextBox textBoxVarInterval39;
        private System.Windows.Forms.TextBox textBoxVarInterval40;
        private System.Windows.Forms.TextBox textBoxVarInterval41;
        private System.Windows.Forms.TextBox textBoxVarInterval42;
        private System.Windows.Forms.TextBox textBoxVarInterval43;
        private System.Windows.Forms.TextBox textBoxVarInterval44;
        private System.Windows.Forms.TextBox textBoxVarInterval45;
        private System.Windows.Forms.TextBox textBoxVarInterval46;
        private System.Windows.Forms.TextBox textBoxVarInterval47;
        private System.Windows.Forms.TextBox textBoxVarInterval48;
        private System.Windows.Forms.TextBox textBoxVarInterval49;
        private System.Windows.Forms.TextBox textBoxVarInterval50;
        private System.Windows.Forms.Button buttonSaveConfig;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxConfig;
        private System.Windows.Forms.DataGridView dataGridViewDataRead;
    }
}

