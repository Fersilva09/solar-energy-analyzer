﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarEnergyPrevision
{
    public class MeasuredData
    {
        public DateTime measuredDateTime;
        public String radiation;
        public String inmetRadiation;
        public String secondInmetRadiation;
        public String neovileInv;
        public String evInv;

    }
}
