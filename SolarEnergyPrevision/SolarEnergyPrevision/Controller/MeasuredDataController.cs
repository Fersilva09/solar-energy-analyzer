﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SolarEnergyPrevision
{
    public class MeasuredDataController
    {
        int neoVilePotRow;
        int neoVilePotCol;


        int invPotRow;
        int invPotCol;

        int inmetRadRow;
        int inmetRadCol;

        double dayValue;

        #region EXCELREAD

        private void GetSavedMeasuredData(List<MeasuredDataDays> measuredDataDays)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("MeasuredData.xml");

            XmlNodeList measureData = xmlDoc.SelectNodes("//MeasuredData//MeasureDate");


            foreach (XmlNode dayData in measureData)
            {
                MeasuredDataDays measuredDataDay = new MeasuredDataDays();
                String day = dayData.Attributes["Day"].Value.ToString();
                measuredDataDay.measuredDay = DateTime.Parse(day);

                XmlNodeList hourData = dayData.ChildNodes;
                foreach (XmlNode measuredValue in hourData)
                {
                    MeasuredData measured = new MeasuredData();
                    measured.measuredDateTime = DateTime.Parse(day + " " + measuredValue.Attributes["Hour"].Value);
                    measured.radiation = measuredValue.Attributes["Radiation"].Value;
                    measured.inmetRadiation = measuredValue.Attributes["inmetRadiation"].Value;
                    measured.secondInmetRadiation = measuredValue.Attributes["secondInmetRadiation"].Value;
                    measured.neovileInv = measuredValue.Attributes["neovileInv"].Value;
                    measured.evInv = measuredValue.Attributes["evInv"].Value;
                    measuredDataDay.measuredHours.Add(measured);
                }

                measuredDataDays.Add(measuredDataDay);
            }
        }

        private void GetExcelData(string sheet, List<MeasuredDataDays> measuredDataDays)
        {
            String fileName = @"C:\Users\Fernando-PC\source\repos\ExcelReader\ExcelReader\bin\Debug\Dados de Irradiação Paineis NeoVille e Centro - 28-08-2018.xlsx";

            MeasuredDataDays measuredDataDay = new MeasuredDataDays();

            Application xlApp = new Application();
            Workbook xlWorkbook = xlApp.Workbooks.Open(fileName);
            _Worksheet xlWorksheet = xlWorkbook.Sheets[sheet];
            Range xlRange = xlWorksheet.UsedRange;

            int numberOfRows = xlRange.Rows.Count;
            int numberOfCols = xlRange.Columns.Count;
            int rowWriter = 1;

            double dateTimeInt = 0;

            bool newHour = false;
            DateTime dateTimeHour = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime valueDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            

            neoVilePotRow = numberOfRows;
            neoVilePotCol = 13;


            invPotRow = numberOfRows;
            invPotCol = 17;


            inmetRadRow = numberOfRows;
            inmetRadCol = 7;
        
            String dateTimeValue = xlRange.Cells[3, 1].Value2.ToString();
            Double dateInt = Double.Parse(dateTimeValue);
            DateTime measureDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((dateInt - 25569) * 86400);
            measuredDataDay.measuredDay = measureDateTime;
            MeasuredData measuredData = null;

            for (int rowCount = 3; rowCount <= numberOfRows; rowCount++)
            {
                for (int colCount = 2; colCount <= 3; colCount++)
                {
                    if (xlRange.Cells[rowCount, colCount] != null && xlRange.Cells[rowCount, colCount].Value2 != null)
                    {
                        String value = xlRange.Cells[rowCount, colCount].Value2.ToString();
                        if (colCount == 2)
                        {
                            dateTimeInt = dateInt + Double.Parse(value);
                            valueDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((dateTimeInt - 25569) * 86400);
                            String dateTime = valueDateTime.ToString();
                            if (!newHour && dateTimeHour.Hour == valueDateTime.Hour)
                            {
                                measuredData = new MeasuredData();
                                measuredData.measuredDateTime = valueDateTime;
                                newHour = true;
                                dateTimeHour = dateTimeHour.AddHours(1);
                            }


                        }
                        if (colCount == 3)
                        {
                            if (newHour)
                            {
                                measuredData.radiation = value;
                                measuredData.neovileInv = GetNeoVileData(xlRange, valueDateTime.Hour);
                                measuredData.evInv = GetInvData(xlRange, valueDateTime.Hour);
                                measuredData.inmetRadiation = GetInmetData(xlRange, valueDateTime.Hour);
                                if (Double.Parse(measuredData.inmetRadiation.ToString()) < 0)
                                {
                                    measuredData.secondInmetRadiation = "0";
                                }
                                else
                                {
                                    String inmetValue = ((Double.Parse(measuredData.inmetRadiation.ToString())) / 3600).ToString();
                                    measuredData.secondInmetRadiation = inmetValue;
                                }
                                rowWriter++;
                                measuredDataDay.measuredHours.Add(measuredData);
                                newHour = false;
                            }

                        }

                    }
                }
            }


            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
            measuredDataDays.Add(measuredDataDay);
        }

        private void SaveMeasuredData(List<MeasuredDataDays> measuredDataDays)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode = xmlDoc.CreateElement("MeasuredData");


            foreach (MeasuredDataDays measureDataDays in measuredDataDays)
            {
                XmlNode dayNode = xmlDoc.CreateElement("MeasureDate");

                XmlAttribute measureDate = xmlDoc.CreateAttribute("Day");
                measureDate.Value = measureDataDays.measuredDay.ToString("yyyy-MM-dd");

                dayNode.Attributes.Append(measureDate);

                foreach (MeasuredData measuredData in measureDataDays.measuredHours)
                {

                    XmlNode hourNode = xmlDoc.CreateElement("MeasureHour");

                    XmlAttribute measureHourValue = xmlDoc.CreateAttribute("Hour");
                    measureHourValue.Value = measuredData.measuredDateTime.ToString("HH:mm:ss");

                    XmlAttribute radiationValue = xmlDoc.CreateAttribute("Radiation");
                    radiationValue.Value = measuredData.radiation;

                    XmlAttribute inmetRadiationValue = xmlDoc.CreateAttribute("inmetRadiation");
                    inmetRadiationValue.Value = measuredData.inmetRadiation;

                    XmlAttribute secondInmetRadiationValue = xmlDoc.CreateAttribute("secondInmetRadiation");
                    secondInmetRadiationValue.Value = measuredData.secondInmetRadiation;

                    XmlAttribute neovileInvValue = xmlDoc.CreateAttribute("neovileInv");
                    neovileInvValue.Value = measuredData.neovileInv;

                    XmlAttribute evInvValue = xmlDoc.CreateAttribute("evInv");
                    evInvValue.Value = measuredData.evInv;

                    hourNode.Attributes.Append(measureHourValue);
                    hourNode.Attributes.Append(radiationValue);
                    hourNode.Attributes.Append(inmetRadiationValue);
                    hourNode.Attributes.Append(secondInmetRadiationValue);
                    hourNode.Attributes.Append(neovileInvValue);
                    hourNode.Attributes.Append(evInvValue);

                    dayNode.AppendChild(hourNode);

                }
                rootNode.AppendChild(dayNode);
            }

            xmlDoc.AppendChild(rootNode);
            xmlDoc.Save("MeasuredData.xml");
        }

        private String GetInmetData(Range xlRange, int hour)
        {
            double dateTimeInt = 0;
            bool newHour = false;
            String invValue = "0";

            for (int rowCount = 4; rowCount <= inmetRadRow; rowCount++)
            {
                for (int colCount = inmetRadCol; colCount <= inmetRadCol + 1; colCount++)
                {
                    if (xlRange.Cells[rowCount, colCount] != null && xlRange.Cells[rowCount, colCount].Value2 != null)
                    {
                        String value = xlRange.Cells[rowCount, colCount].Value2.ToString();
                        if (colCount == 7)
                        {
                            dateTimeInt = Double.Parse(value);
                            if (dateTimeInt == hour)
                                newHour = true;

                        }
                        if (colCount == 8)
                        {
                            if (newHour)
                            {
                                invValue = value;
                                rowCount = inmetRadRow;
                                newHour = false;
                            }

                        }

                    }
                    else
                    {
                        inmetRadRow = rowCount - 1;
                    }
                }
            }

            return invValue;
        }

        private String GetInvData(Range xlRange, int hour)
        {
            double dateTimeInt = 0;
            bool newHour = false;
            String invValue = "0";

            for (int rowCount = 3; rowCount <= invPotRow; rowCount++)
            {
                for (int colCount = invPotCol; colCount <= invPotCol + 2; colCount++)
                {
                    if (xlRange.Cells[rowCount, colCount] != null && xlRange.Cells[rowCount, colCount].Value2 != null)
                    {
                        String value = xlRange.Cells[rowCount, colCount].Value2.ToString();
                        if (colCount == 17)
                        {
                            dateTimeInt += Double.Parse(value);
                            DateTime valueDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((dateTimeInt - 25569) * 86400);
                            String dateTime = valueDateTime.ToString();
                            if (valueDateTime.Hour == hour)
                                newHour = true;

                        }
                        if (colCount == 18)
                        {
                            if (newHour)
                            {
                                invValue = value;
                                rowCount = invPotRow;
                                newHour = false;
                            }

                        }

                    }
                    else
                    {
                        invPotRow = rowCount - 1;
                    }
                }
            }

            return invValue;
        }

        private String GetNeoVileData(Range xlRange, int hour)
        {

            double dateTimeInt = 0;
            bool newHour = false;
            String neoVileValue = "0";

            for (int rowCount = 3; rowCount <= neoVilePotRow; rowCount++)
            {
                for (int colCount = neoVilePotCol; colCount <= neoVilePotCol + 2; colCount++)
                {
                    if (xlRange.Cells[rowCount, colCount] != null && xlRange.Cells[rowCount, colCount].Value2 != null)
                    {
                        String value = xlRange.Cells[rowCount, colCount].Value2.ToString();
                        if (colCount == 13)
                        {
                            dateTimeInt += Double.Parse(value);
                            DateTime valueDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds((dateTimeInt - 25569) * 86400);
                            String dateTime = valueDateTime.ToString();
                            if (valueDateTime.Hour == hour)
                                newHour = true;

                        }
                        if (colCount == 14)
                        {
                            if (newHour)
                            {
                                neoVileValue = value;
                                rowCount = neoVilePotRow;
                                newHour = false;
                            }

                        }

                    }
                    else
                    {
                        neoVilePotRow = rowCount - 1;
                    }
                }
            }

            return neoVileValue;

        }

        internal bool GetData(List<MeasuredDataDays> measuredDataDays)
        {
            bool result = true;
            try
            {
                GetSavedMeasuredData(measuredDataDays);

                String fileName = Directory.GetCurrentDirectory() + @"\Dados de Irradiação Paineis NeoVille e Centro - 28-08-2018.xlsx";

                Application xlApp = new Application();
                Workbook xlWorkbook = xlApp.Workbooks.Open(fileName);

                foreach (Worksheet worksheet in xlWorkbook.Worksheets)
                {
                    String sheetName = worksheet.Name;
                    String sheetDay = sheetName.Split('_')[0];
                    String sheetMonth = sheetName.Split('_')[1];
                    bool newData = true;

                    foreach (MeasuredDataDays measuredDay in measuredDataDays)
                    {
                        if (measuredDay.measuredDay.Month == Int32.Parse(sheetMonth))
                        {
                            if (measuredDay.measuredDay.Day == Int32.Parse(sheetDay))
                            {
                                newData = false;
                            }
                        }
                    }

                    if (newData)
                        GetExcelData(sheetName, measuredDataDays);
                }
                SaveMeasuredData(measuredDataDays);
            }
            catch(Exception exe)
            {
                result = false;
            }

           

            return result;
        }

        #endregion
    }
}
