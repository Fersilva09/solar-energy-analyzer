﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarEnergyPrevision
{
    public class PrevisionDataDays
    {
     
        public enum Month
        {
            Jan = 1,
            Fev,
            Mar,
            Abr,
            Mai,
            Jun,
            Jul,
            Ago,
            Set,
            Out,
            Nov,
            Dez
        };

        const int monthPosition = 2;
        public String previsionActualDay;                //Data em que foi realizada a medicao
        public String prevesionDay;                      //Data que esta sendo prevista

        public List<PrevisionDataHours> hourPrevision = new List<PrevisionDataHours>();       //List dos varios horarios em que foram feitas medicoes


        public DateTime ConvertStringToDateTime()
        {
            String dateTimeStringMonth = prevesionDay.Split(' ')[monthPosition];
            int month = (int)((Month)Enum.Parse(typeof(Month), dateTimeStringMonth));
            String formatedPrevisionDay = prevesionDay.Split(' ').First() + "-" + month.ToString() + "-" + prevesionDay.Split(' ').Last();
            DateTime dayPrevisionDate = DateTime.ParseExact(formatedPrevisionDay, "d-M-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            return dayPrevisionDate;
        }
    }
}
