﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace SolarEnergyPrevision
{
    public class PrevisionDataController
    {
        private Thread thread;
        private bool run;

        private DateTime requestTime = DateTime.Now;
        List<PrevisionDataDays> PrevisionDataDaysList = new List<PrevisionDataDays>();

        const int DayLenghtHours = 24;



        public PrevisionDataController()
        {

        }


        public bool StartCapturePrevision()
        {
            bool result = true;
            try
            {
                LoadData();
                GetPrevisionData();
                run = true;
                thread = new Thread(GetRequestTime);
                thread.Start();
            }
            catch(Exception exe)
            {
                result = false;
            }

            return result;
            
        }

        public void GetData(List<PrevisionDataDays> previsionDataDays)
        {
            foreach(PrevisionDataDays prevision in PrevisionDataDaysList)
            {
                bool newPrevision = true;
                foreach(PrevisionDataDays oldPrevision in previsionDataDays)
                {
                    if(oldPrevision == prevision)
                    {
                        newPrevision = false;
                    }
                }

                if (newPrevision)
                    previsionDataDays.Add(prevision);
            }
        }


        public void GetRequestTime()
        {
            while(run)
            {
                if (requestTime.AddHours(1) <= DateTime.Now)
                {
                    GetPrevisionData();
                    requestTime = DateTime.Now;
                }
            }
          
        }

        public bool Disconnect()
        {
            bool result = true;
            try
            {
                this.run = false;
                thread.Join();
            }
            catch
            {
                result = false;

            }
            return result;
           
        }




        #region GETDATA

        private void LoadData()
        {

            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("PrevisionData.xml");


                XmlNodeList daysPrevision = xmlDoc.SelectNodes("PrevisionData");


                foreach (XmlNode dayNode in daysPrevision[0].ChildNodes)
                {

                    XmlNodeList hourPrevision = dayNode.ChildNodes;
                    PrevisionDataDays day = new PrevisionDataDays();
                    day.prevesionDay = dayNode.Attributes["data"].Value;

                    foreach (XmlNode hourNode in hourPrevision)
                    {
                        XmlNodeList hourData = hourNode.ChildNodes;
                        PrevisionDataHours hour = new PrevisionDataHours();
                        hour.previsionHour = hourNode.Attributes["Horario_Medicao"].Value;
                        foreach (XmlNode previsionNode in hourData)
                        {


                            PrevisionData prevision = new PrevisionData();
                            prevision.time = previsionNode.Attributes["Hora"].Value;
                            prevision.clouds = previsionNode.Attributes["Nuvens"].Value;
                            prevision.temp = previsionNode.Attributes["Temperatura"].Value;
                            prevision.acPrec = previsionNode.Attributes["Precipitacao_Acumulada"].Value;
                            prevision.wind = previsionNode.Attributes["Vento"].Value;
                            prevision.termSensation = previsionNode.Attributes["Sensaocao_Termica"].Value;
                            prevision.relativeHumidity = previsionNode.Attributes["Umidade_Relativa"].Value;
                            prevision.rainChance = previsionNode.Attributes["Probabilidade_de_Precipitacao"].Value;
                            prevision.windStrong = previsionNode.Attributes["Rajada_de_Vento"].Value;
                            prevision.reducPression = previsionNode.Attributes["Pressao_Reduzida"].Value;
                            prevision.radiation = previsionNode.Attributes["Indice_UV"].Value;
                            prevision.visibility = previsionNode.Attributes["Visibilidade"].Value;
                            hour.prevision.Add(prevision);
                        }
                        day.hourPrevision.Add(hour);

                    }

                    PrevisionDataDaysList.Add(day);
                }
            }
            catch (Exception exe)
            {

            }
        }
        private void GetPrevisionData()
        {
            try
            {

                String weekDay = (DateTime.Now.ToString("ddd", new CultureInfo("pt-BR")));
                weekDay = weekDay[0].ToString().ToUpper() + weekDay.Substring(1);
                String month = ((PrevisionDataDays.Month)((DateTime.Now.Month))).ToString().Substring(0, 3);
                String today = DateTime.Now.Day.ToString() + " de " + month + " de " + DateTime.Now.Year.ToString();

                String weekNextDay = (DateTime.Now.AddDays(1).ToString("ddd", new CultureInfo("pt-BR")));
                weekNextDay = weekNextDay[0].ToString().ToUpper() + weekNextDay.Substring(1);
                String nextDayMonth = ((PrevisionDataDays.Month)((DateTime.Now.AddDays(1).Month))).ToString().Substring(0, 3);
                String nextDay = DateTime.Now.AddDays(1).Day.ToString() + " de " + nextDayMonth + " de " + DateTime.Now.AddDays(1).Year.ToString();

                #region WEB_REQUEST
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.simepar.br/prognozweb/simepar/forecast_by_counties/4106902");

                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                String responseString = readStream.ReadToEnd();

                #endregion

                response.Close();
                readStream.Close();
                Console.Write(responseString);
                int index = responseString.IndexOf("HOURLY INFORMATIONS ABOUT THE SELECTED DAY");
                String dayInfo = responseString.Substring(index);

                index = dayInfo.IndexOf(nextDay);
                dayInfo = dayInfo.Substring(index);

                bool newDay = true;
                PrevisionDataDays newDayToAdd = null;
                foreach (PrevisionDataDays day in PrevisionDataDaysList)
                {
                    if (day.prevesionDay == nextDay)
                    {
                        String hourString = DateTime.Now.Hour.ToString() + ":00";
                        bool newHour = true;
                        foreach (PrevisionDataHours hour in day.hourPrevision)
                        {
                            if (hourString == hour.previsionHour)
                                newHour = false;
                        }
                        if (newHour)
                        {
                            PrevisionDataHours newHourPrevision = new PrevisionDataHours();
                            newHourPrevision.previsionHour = hourString;
                            newHourPrevision.prevision = GetPrevision(dayInfo);
                            day.hourPrevision.Add(newHourPrevision);
                            newDayToAdd = day;
                        }
                        newDay = false;
                    }
                }

                if (newDay)
                {
                    PrevisionDataDays newDayPrevision = new PrevisionDataDays();
                    newDayPrevision.previsionActualDay = today;
                    newDayPrevision.prevesionDay = nextDay;

                    PrevisionDataHours newHourPrevision = new PrevisionDataHours();
                    newHourPrevision.previsionHour = DateTime.Now.Hour.ToString() + ":00";
                    newHourPrevision.prevision = GetPrevision(dayInfo);

                    newDayPrevision.hourPrevision.Add(newHourPrevision);
                    PrevisionDataDaysList.Add(newDayPrevision);
                    newDayToAdd = newDayPrevision;
                }

                AppendData(newDayToAdd, newDay);
            }
            catch (Exception ex)
            {

            }

        }

        private List<PrevisionData> GetPrevision(String dayInfo)
        {
            List<PrevisionData> nextDayHourPrevision = new List<PrevisionData>();

            for (int counter = 0; counter < DayLenghtHours; counter++)
            {
                int hourIndex = 0;
                int nextHourIndex = 0;
                int valueIndex = 0;
                int endValueIndex = 0;

                String hourInfo = "";
                String value;
                String nextSeparatorText;
                String separatorText;
                if (counter < 10)
                    separatorText = "collap_1_0" + counter.ToString();
                else
                    separatorText = "collap_1_" + counter.ToString();
                if (counter < 9)
                    nextSeparatorText = "collap_1_0" + (counter + 1).ToString();
                else
                    nextSeparatorText = "collap_1_" + (counter + 1).ToString();


                PrevisionData prevision = new PrevisionData();

                if (counter != DayLenghtHours - 1)
                {
                    value = "";
                    hourIndex = dayInfo.IndexOf(separatorText);
                    nextHourIndex = dayInfo.IndexOf(nextSeparatorText);
                    hourInfo = dayInfo.Substring(hourIndex, (nextHourIndex - hourIndex));
                    hourInfo = hourInfo.Replace("\t", "");
                    hourInfo = hourInfo.Replace("\n", "");
                    hourInfo = hourInfo.Replace("\r", "");
                }
                else
                {
                    hourIndex = dayInfo.IndexOf(separatorText);
                    hourInfo = dayInfo.Substring(hourIndex);
                    hourInfo = hourInfo.Replace("\t", "");
                    hourInfo = hourInfo.Replace("\n", "");
                    hourInfo = hourInfo.Replace("\r", "");
                }
                valueIndex = (hourInfo.IndexOf("<div class=\"ah-time\">")) + "<div class=\"ah-time\">".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Substring(0, value.IndexOf("</div>"));
                prevision.time = value;

                valueIndex = (hourInfo.IndexOf("title=\"")) + "title=\"".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Substring(0, value.IndexOf("\""));
                prevision.clouds = value;

                valueIndex = (hourInfo.IndexOf("</i>")) + "</i>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Substring(0, value.IndexOf("</div>"));
                prevision.temp = value;

                valueIndex = (hourInfo.IndexOf("<div class=\"ah-prec\">")) + "<div class=\"ah-prec\">".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Substring(0, value.IndexOf("</div>"));
                prevision.acPrec = value;

                valueIndex = (hourInfo.IndexOf("<div class=\"ah-wind\">")) + "<div class=\"ah - wind\">".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Substring(0, value.IndexOf("</div>"));
                prevision.wind = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Sensação Térmica:</span>")) + "<span class=\"var\">Sensação Térmica:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.termSensation = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Umidade Relativa:</span>")) + "<span class=\"var\">Umidade Relativa:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.relativeHumidity = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Probabilidade de Precipitação:</span>")) + "<span class=\"var\">Probabilidade de Precipitação:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.rainChance = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Rajada:</span>")) + "<span class=\"var\">Rajada:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.windStrong = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Pressão Reduzida:</span>")) + "<span class=\"var\">Pressão Reduzida:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.reducPression = value;

                valueIndex = (hourInfo.IndexOf(" <span class=\"var\">Índice UV:</span>")) + " <span class=\"var\">Índice UV:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.radiation = value;

                valueIndex = (hourInfo.IndexOf("<span class=\"var\">Visibilidade:</span>")) + "<span class=\"var\">Visibilidade:</span>".Length;
                value = hourInfo.Substring(valueIndex);
                value = value.Replace(" ", "");
                valueIndex = (value.IndexOf("\"val\">") + "\"val\">".Length);
                endValueIndex = value.IndexOf("</span>") - valueIndex;
                value = value.Substring(valueIndex, endValueIndex);
                prevision.visibility = value;

                nextDayHourPrevision.Add(prevision);
            }

            return nextDayHourPrevision;
        }

        private void AppendData(PrevisionDataDays day, bool newDay)
        {
            if (day != null)
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load("PrevisionData.xml");
                XmlNode rootNode = xmlDoc.FirstChild;

                if (newDay)
                {
                    XmlNode previsionDayNode = xmlDoc.CreateElement("dia");
                    XmlAttribute measureDate = xmlDoc.CreateAttribute("data");
                    measureDate.Value = day.prevesionDay;
                    previsionDayNode.Attributes.Append(measureDate);

                    XmlNode measureNode = xmlDoc.CreateElement("Medicao");
                    XmlAttribute measureTime = xmlDoc.CreateAttribute("Horario_Medicao");
                    measureTime.Value = day.hourPrevision.Last().previsionHour;
                    measureNode.Attributes.Append(measureTime);

                    CreateMeasureNode(measureNode, day, xmlDoc);

                    previsionDayNode.AppendChild(measureNode);
                    rootNode.AppendChild(previsionDayNode);
                }
                else
                {
                    XmlNodeList previsionDayNodes = xmlDoc.SelectNodes("PrevisionData");
                    XmlNode previsionDayNode = null;
                    foreach (XmlNode node in previsionDayNodes[0].ChildNodes)
                    {
                        if (node.Attributes["data"].Value == day.prevesionDay)
                            previsionDayNode = node;
                    }

                    XmlNode measureNode = xmlDoc.CreateElement("Medicao");
                    XmlAttribute measureTime = xmlDoc.CreateAttribute("Horario_Medicao");
                    measureTime.Value = day.hourPrevision.Last().previsionHour;
                    measureNode.Attributes.Append(measureTime);

                    CreateMeasureNode(measureNode, day, xmlDoc);
                    previsionDayNode.AppendChild(measureNode);
                }

                xmlDoc.Save("PrevisionData.xml");
            }
        }

        private void CreateMeasureNode(XmlNode measureNode, PrevisionDataDays day, XmlDocument xmlDoc)
        {
            foreach (PrevisionData prevision in day.hourPrevision.Last().prevision)
            {
                XmlNode hourNode = xmlDoc.CreateElement("Time");

                XmlAttribute time = xmlDoc.CreateAttribute("Hora");
                time.Value = prevision.time.ToString();

                XmlAttribute clouds = xmlDoc.CreateAttribute("Nuvens");
                clouds.Value = prevision.clouds.ToString();

                XmlAttribute temp = xmlDoc.CreateAttribute("Temperatura");
                temp.Value = prevision.temp.ToString();

                XmlAttribute acPrec = xmlDoc.CreateAttribute("Precipitacao_Acumulada");
                acPrec.Value = prevision.acPrec.ToString();

                XmlAttribute wind = xmlDoc.CreateAttribute("Vento");
                wind.Value = prevision.wind.ToString();

                XmlAttribute termSensation = xmlDoc.CreateAttribute("Sensaocao_Termica");
                termSensation.Value = prevision.termSensation.ToString();

                XmlAttribute relativeHumidity = xmlDoc.CreateAttribute("Umidade_Relativa");
                relativeHumidity.Value = prevision.relativeHumidity.ToString();

                XmlAttribute rainChance = xmlDoc.CreateAttribute("Probabilidade_de_Precipitacao");
                rainChance.Value = prevision.rainChance.ToString();

                XmlAttribute windStrong = xmlDoc.CreateAttribute("Rajada_de_Vento");
                windStrong.Value = prevision.windStrong.ToString();

                XmlAttribute reducPression = xmlDoc.CreateAttribute("Pressao_Reduzida");
                reducPression.Value = prevision.reducPression.ToString();

                XmlAttribute radiation = xmlDoc.CreateAttribute("Indice_UV");
                radiation.Value = prevision.radiation.ToString();

                XmlAttribute visibility = xmlDoc.CreateAttribute("Visibilidade");
                visibility.Value = prevision.visibility.ToString();

                hourNode.Attributes.Append(time);
                hourNode.Attributes.Append(clouds);
                hourNode.Attributes.Append(temp);
                hourNode.Attributes.Append(acPrec);
                hourNode.Attributes.Append(wind);
                hourNode.Attributes.Append(termSensation);
                hourNode.Attributes.Append(relativeHumidity);
                hourNode.Attributes.Append(rainChance);
                hourNode.Attributes.Append(windStrong);
                hourNode.Attributes.Append(reducPression);
                hourNode.Attributes.Append(radiation);
                hourNode.Attributes.Append(visibility);
                measureNode.AppendChild(hourNode);

            }
        }

        #endregion
    }
}
