﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarEnergyPrevision
{
    public class MeasuredDataDays
    {
        public DateTime measuredDay;
        public List<MeasuredData> measuredHours = new List<MeasuredData>();
    }
}
