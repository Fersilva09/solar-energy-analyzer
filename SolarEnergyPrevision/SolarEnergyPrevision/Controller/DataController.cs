﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarEnergyPrevision
{
    public class DataController
    {
        private List<PrevisionDataDays> dayPrevisions = new List<PrevisionDataDays>();
        private List<MeasuredDataDays> measuredDataDays = new List<MeasuredDataDays>();

        public List<String> dataDateTime = new List<String>();
        public List<String> radiation = new List<String>();
        public List<String> potency = new List<String>();
        public List<String> temperature = new List<String>();
        public List<String> uvRadiation = new List<String>();
        public List<String> rainChance = new List<String>();
        public List<String> clouds = new List<String>();
        public List<String> windSpeed = new List<String>();
        public List<String> thermalSensation = new List<String>();
        public List<String> airHumidity = new List<String>();
        public List<String> reducPression = new List<String>();
        public List<String> wind = new List<String>();
        public List<String> accRain = new List<String>();
        public List<String> visibility = new List<String>();
        public List<String> sun = new List<String>();
        bool sunRule = false;

        private MeasuredDataController measuredDataController = new MeasuredDataController();
        private PrevisionDataController previsionDataController = new PrevisionDataController();

        public DataController()
        {

            
           
        }

        public bool SetDataReader(DateTime startDateTime, DateTime endDateTime)
        {
            measuredDataController.GetData(measuredDataDays);
            previsionDataController.GetData(dayPrevisions);
            bool thereIsOneDayMeasured = false;
            bool thereIsTwoDaysMeasured = false;
            bool thereIsOneDayPrevision = false;
            bool thereIsTwoDaysPrevision = false;
            bool result = false;

            MeasuredDataDays measuredDayOne = new MeasuredDataDays();
            MeasuredDataDays measuredDayTwo = new MeasuredDataDays();

            PrevisionDataDays dayPrevisionOne = new PrevisionDataDays();
            PrevisionDataDays dayPrevisionTwo = new PrevisionDataDays();


            bool twoDays = (startDateTime.Day != endDateTime.Day);
            if (twoDays)
            {
                foreach (MeasuredDataDays measureDataDay in measuredDataDays)
                {
                    if (measureDataDay.measuredDay.Date == startDateTime.Date || measureDataDay.measuredDay.Date == endDateTime.Date)
                    {
                        if (thereIsOneDayMeasured)
                        {
                            thereIsTwoDaysMeasured = true;
                            measuredDayTwo = measureDataDay;
                        }
                        else
                        {
                            thereIsOneDayMeasured = true;
                            measuredDayOne = measureDataDay;
                        }

                    }
                }

                foreach (PrevisionDataDays dayPrevision in dayPrevisions)
                {
                    if (dayPrevision.ConvertStringToDateTime().Date == startDateTime.Date || dayPrevision.ConvertStringToDateTime().Date == endDateTime.Date)
                    {
                        if (thereIsOneDayPrevision)
                        {
                            thereIsTwoDaysPrevision = true;
                            dayPrevisionTwo = dayPrevision;
                        }
                        else
                        {
                            thereIsOneDayPrevision = true;
                            dayPrevisionOne = dayPrevision;
                        }

                    }
                }
            }
            else
            {
                foreach (MeasuredDataDays measureDataDay in measuredDataDays)
                {
                    if (measureDataDay.measuredDay.Date == startDateTime.Date || measureDataDay.measuredDay.Date == endDateTime.Date)
                    {
                        thereIsOneDayMeasured = true;
                        measuredDayOne = measureDataDay;
                    }
                }

                foreach (PrevisionDataDays dayPrevision in dayPrevisions)
                {
                    if (dayPrevision.ConvertStringToDateTime().Date == startDateTime.Date || dayPrevision.ConvertStringToDateTime().Date == endDateTime.Date)
                    {
                        thereIsOneDayPrevision = true;
                        dayPrevisionOne = dayPrevision;
                    }
                }

            }

            if (twoDays)
            {
                
                if (thereIsTwoDaysMeasured && thereIsTwoDaysPrevision)
                {
                    result = true;
                    foreach (PrevisionData prevision in dayPrevisionOne.hourPrevision.Last().prevision)
                    {
                        this.dataDateTime.Add(dayPrevisionOne.prevesionDay + " " + prevision.time);
                        this.uvRadiation.Add(prevision.radiation);
                        this.temperature.Add(prevision.temp);
                        this.rainChance.Add(prevision.rainChance);
                        this.reducPression.Add(prevision.reducPression);
                        this.thermalSensation.Add(prevision.termSensation);
                        this.reducPression.Add(prevision.reducPression);
                        this.airHumidity.Add(prevision.relativeHumidity);
                        this.accRain.Add(prevision.acPrec);
                        this.clouds.Add(prevision.clouds);
                        this.wind.Add(prevision.windStrong);
                        this.windSpeed.Add(prevision.wind);
                        this.visibility.Add(prevision.visibility);
                    }
                    foreach (PrevisionData prevision in dayPrevisionTwo.hourPrevision.Last().prevision)
                    {
                        this.dataDateTime.Add(dayPrevisionTwo.prevesionDay + " " + prevision.time);
                        this.uvRadiation.Add(prevision.radiation);
                        this.temperature.Add(prevision.temp);
                        this.rainChance.Add(prevision.rainChance);
                        this.reducPression.Add(prevision.reducPression);
                        this.thermalSensation.Add(prevision.termSensation);
                        this.reducPression.Add(prevision.reducPression);
                        this.airHumidity.Add(prevision.relativeHumidity);
                        this.accRain.Add(prevision.acPrec);
                        this.clouds.Add(prevision.clouds);
                        this.wind.Add(prevision.windStrong);
                        this.windSpeed.Add(prevision.wind);
                        this.visibility.Add(prevision.visibility);
                    }
                    foreach (MeasuredData measuredDate in measuredDayOne.measuredHours)
                    {
                        this.radiation.Add(measuredDate.radiation);
                        this.potency.Add(measuredDate.evInv);
                    }
                    foreach (MeasuredData measuredDate in measuredDayTwo.measuredHours)
                    {
                        this.radiation.Add(measuredDate.radiation);
                        this.potency.Add(measuredDate.evInv);
                    }
                }
            }
            else
            {
               
                if (thereIsOneDayMeasured && thereIsOneDayPrevision)
                {
                    result = true;
                    foreach (PrevisionData prevision in dayPrevisionOne.hourPrevision.Last().prevision)
                    {
                        this.dataDateTime.Add(dayPrevisionOne.prevesionDay + " " + prevision.time);
                        this.uvRadiation.Add(prevision.radiation);
                        this.temperature.Add(prevision.temp);
                        this.rainChance.Add(prevision.rainChance);
                        this.reducPression.Add(prevision.reducPression);
                        this.thermalSensation.Add(prevision.termSensation);
                        this.reducPression.Add(prevision.reducPression);
                        this.airHumidity.Add(prevision.relativeHumidity);
                        this.accRain.Add(prevision.acPrec);
                        this.clouds.Add(prevision.clouds);
                        this.wind.Add(prevision.windStrong);
                        this.windSpeed.Add(prevision.wind);
                        this.visibility.Add(prevision.visibility);
                    }
                    foreach (MeasuredData measuredDate in measuredDayOne.measuredHours)
                    {
                        this.radiation.Add(measuredDate.radiation);
                        this.potency.Add(measuredDate.evInv);
                    }
                }
            }

            return result;
        }

        public bool GetPrevisionData()
        {
            bool result = measuredDataController.GetData(measuredDataDays);
            return result;
        }

        public bool StartCapture()
        {
            bool result = previsionDataController.StartCapturePrevision();
            return result;
        }

        public bool StopCapture()
        {
            bool result = previsionDataController.Disconnect();
            return result;
        }
    }

}
