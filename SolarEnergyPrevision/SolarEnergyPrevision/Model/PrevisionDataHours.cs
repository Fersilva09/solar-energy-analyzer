﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SolarEnergyPrevision
{
    public class PrevisionDataHours
    {
        public String previsionHour;                                                                                   //Hora em que foi realizada a previsao
        public List<PrevisionData> prevision = new List<PrevisionData>();         //Previsao das proximas 24 horas
    }
}
